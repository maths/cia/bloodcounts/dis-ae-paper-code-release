# Dis-AE - Paper Repository

![Dis-AE model architecture](DisAE_Diagramme.png)



This is the repository for the code used in the Dis-AE paper.
All code is written by Samuel Tull and Daniel Kreuter.
The code is stored using a Python package-like structure.
The functions and classes can be used by including the files in your script. E.g.
```
from disentanglement.models import Sam_DisAE
```
to import the Dis-AE neural network model class.

## Requirements
The code was run both on MacOS Ventura 13.4 and a high-performance computing environment running Scientific Linux release 7.9 (Nitrogen). The Python version was 3.9.
You can install the necessary packages by running
```
pip install -r requirements.txt
```
This should take no more than a few minutes on a typical desktop computer.

## Available Files

### make_datasets
Folder containing the scripts to create the synthetic datasets and perform pre-filtering on the clinical datasets.

### datasets
Folder in which all the datasets should be saved (only the synthetic ones are provided at [link] for privacy reasons).

### disentanglement
Folder containing all code files including the Dis-AE model code and evaluation functions used in the paper.

### experiments
Folder containing scripts and results for all experiments in the paper. The trained models from the experiments reported in the paper are also saved.

## Example Demo
In this demo we will create the synthetic dataset A and run the full experiments of the paper on this dataset. Please first install the required packages as explained above.

1. Go into the folder containing the dataset creation scripts using `cd make_datasets`.
2. To generate dataset A, we can run `python create_dataset_A.py`. This will create a new file `data_A.csv` in the `datasets` folder in the repository root folder. (Running this should take less than five minutes)
3. We can now run the experiment on dataset A. To do this we can change directory to the folder containing the experiments using `cd experiments/datasetA` from the repository root folder.
4. The `datasetA` folder already contains all results of the experiments which are reported in the paper. We can delete everything except for the experiment script by using `rm !(datasetA.py)`.
5. We can now run `python datasetA.py` to run the full experiment and reproduce all results for dataset A. (Due to every experiment running as 5-fold cross-validation with 5 runs per fold, this might take a while; around 1 hour on a typical desktop computer.)

The results are stored per model in `.csv` files and the PHATE plots of all representation spaces in the experiment are saved as `.png` files. The best Dis-AE model during the experiment is saved in a separate folder `best_disae_model` while the best Vanilla AE model is saved as a `.ckpt` PyTorch-Lightning checkpoint file.

All other experiments can be run in the same manner (although they will take longer). Due to privacy reasons only the synthetic dataset experiments can be run from end to end. However, the code for the clinical dataset pre-processing and analysis is provided.