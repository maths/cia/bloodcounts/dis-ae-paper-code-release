import sys

sys.path.append("../../")

import numpy as np
import xgboost as xgb

from joblib import Parallel, delayed

from disentanglement import eval_utils, training_utils


data = training_utils.Dataset(
    data="../../datasets/data_manyaffines.csv",
    X_labels=[str(i) for i in range(32)],
    Y_labels=[
        "ClassCategory_0",
        "Machine",
    ],
)

data.normalise()


def for_step(xgbclasser, highest_variation_label, highest_train_label):
    print(f"Getting variation for {highest_variation_label}...")
    # get variation
    x, y = data.get_filtered_slice(
        label="Machine",
        label_filter=[i for i in range(highest_train_label + 1)]
        + [highest_variation_label],
    )
    variation_kl = eval_utils.model_variation(
        embeddings=x.values,
        tasks_labels=y[["ClassCategory_0"]].values,
        domains_labels=y[["Machine"]].values,
        n_betas=100,
        distance_metric=eval_utils.jensen_shannon_distance_1d,
    )
    # get score
    x, y = data.get_filtered_slice(
        label="Machine",
        label_filter=[highest_variation_label],
    )
    score = xgbclasser.score(x.values, y["ClassCategory_0"].values)
    return (
        highest_variation_label,
        score,
        # variation_wasser,
        # variation_tv,
        variation_kl,
        # variation_mmd,
    )


def train_DisAE_and_get_variations(highest_train_label):
    # get train data
    print(f"Training on {highest_train_label+1} machines...")
    print("Getting train data...")
    X_train, Y_train = data.get_filtered_slice(
        label="Machine",
        label_filter=[i for i in range(highest_train_label + 1)],
    )
    # train a new XGB classer on source
    train_embed_classer = xgb.XGBClassifier(use_label_encoder=False, verbosity=0).fit(
        X_train.values, Y_train["ClassCategory_0"].values
    )
    machine_indices = [highest_train_label]
    variations_kl = [
        eval_utils.model_variation(
            embeddings=X_train.values,
            tasks_labels=Y_train[["ClassCategory_0"]].values,
            domains_labels=Y_train[["Machine"]].values,
            n_betas=100,
            distance_metric=eval_utils.jensen_shannon_distance_1d,
        )
    ]
    scores = [
        train_embed_classer.score(X_train.values, Y_train["ClassCategory_0"].values)
    ]
    print("Getting all other variations...")
    results = Parallel(n_jobs=-1, verbose=100)(
        delayed(
            lambda highest_variation_label: for_step(
                train_embed_classer,
                highest_variation_label,
                highest_train_label,
            )
        )(highest_variation_label)
        for highest_variation_label in np.sort(data.Y["Machine"].unique())[
            (highest_train_label + 1) :
        ]
    )

    for result in results:
        machine_indices.append(result[0])
        scores.append(result[1])
        # variations_wasser.append(result[2])
        # variations_tv.append(result[3])
        variations_kl.append(result[2])
        # variations_mmd.append(result[5])

    return np.stack(
        (
            machine_indices,
            scores,
            # variations_wasser,
            # variations_tv,
            variations_kl,
            # variations_mmd,
            # variations_icp,
        ),
        axis=-1,
    )


# train on 1, 5, 10, 15, 20, 25 and save each result
for highest_train_label in [1, 5, 10, 15, 20, 25]:
    np.save(
        f"results_manyaffines/{highest_train_label+1}_machines_nomodel_variations_score.npy",
        train_DisAE_and_get_variations(highest_train_label),
    )
