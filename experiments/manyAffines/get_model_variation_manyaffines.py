import sys

sys.path.append("../../")

import pandas as pd
import numpy as np
import xgboost as xgb
import torch

from joblib import Parallel, delayed

from disentanglement import models, eval_utils, training_utils


batch_size = 1024
n_betas = 1
random_seed = 12345

data = training_utils.Dataset(
    data="../../datasets/data_manyaffines.csv",
    X_labels=[str(i) for i in range(32)],
    Y_labels=[
        "ClassCategory_0",
        "Machine",
    ],
)

data.normalise()


def for_step(model, xgbclasser, highest_variation_label, highest_train_label):
    print(f"Getting variation for {highest_variation_label}...")
    # get variation
    x, y = data.get_filtered_slice(
        label="Machine",
        label_filter=[i for i in range(highest_train_label + 1)]
        + [highest_variation_label],
    )
    current_embed = model(torch.Tensor(x.values)).detach().numpy()
    # variation_wasser = eval_utils.model_variation(
    #     embeddings=current_embed,
    #     tasks_labels=y[["ClassCategory_0"]].values,
    #     domains_labels=y[["Machine"]].values,
    #     n_betas=1,
    #     # distance_metric=eval_utils.total_variation_prob_distance,
    # )
    # variation_tv = eval_utils.model_variation(
    #     embeddings=current_embed,
    #     tasks_labels=y[["ClassCategory_0"]].values,
    #     domains_labels=y[["Machine"]].values,
    #     n_betas=1,
    #     distance_metric=eval_utils.total_variation_prob_distance,
    # )
    variation_kl = eval_utils.model_variation(
        embeddings=current_embed,
        tasks_labels=y[["ClassCategory_0"]].values,
        domains_labels=y[["Machine"]].values,
        n_betas=n_betas,
        distance_metric=eval_utils.jensen_shannon_divergence_1d,
        random_state=random_seed,
    )
    # variation_mmd = eval_utils.model_variation(
    #     embeddings=current_embed,
    #     tasks_labels=y[["ClassCategory_0"]].values,
    #     domains_labels=y[["Machine"]].values,
    #     n_betas=1,
    #     distance_metric=eval_utils.maximum_mean_discrepancy,
    # )

    # get score
    x, y = data.get_filtered_slice(
        label="Machine",
        label_filter=[highest_variation_label],
    )
    current_embed = model(torch.Tensor(x.values)).detach().numpy()
    score = xgbclasser.score(current_embed, y["ClassCategory_0"].values)
    return (
        highest_variation_label,
        score,
        # variation_wasser,
        # variation_tv,
        variation_kl,
        # variation_mmd,
    )


def train_DisAE_and_get_variations(highest_train_label):
    # get train data
    print(f"Training on {highest_train_label+1} machines...")
    print("Getting train data...")
    X_train, Y_train = data.get_filtered_slice(
        label="Machine",
        label_filter=[i for i in range(highest_train_label + 1)],
    )
    train_data = pd.concat([X_train, Y_train], axis=1)
    # make & train model
    selfdis_ae_model = models.Sam_DisAE(
        heads=[
            ["ClassCategory_0", True, True],
            ["Machine", False, True],
        ],
        layers=[32, 16, 12],
        reconstruction_factor=9.055,
        task_factor=0.3279,
        domain_factor=2.939,
    )
    print("Training model...")
    selfdis_ae_model.fit(
        data=train_data,
        X_columns=[str(i) for i in range(32)],
        batch_size=batch_size,
        weight_decay=0.0001,
        lr_0=0.001,
    )
    # get embeddings and variations
    print("Getting first variation...")
    train_embed = selfdis_ae_model(torch.Tensor(X_train.values))
    # train a new XGB classer on source
    train_embed_classer = xgb.XGBClassifier(use_label_encoder=False, verbosity=0).fit(
        train_embed.detach().numpy(), Y_train["ClassCategory_0"].values
    )
    machine_indices = [highest_train_label]

    # variations_wasser = [
    #     eval_utils.model_variation(
    #         embeddings=train_embed.detach().numpy(),
    #         tasks_labels=Y_train[["ClassCategory_0"]].values,
    #         domains_labels=Y_train[["Machine"]].values,
    #         n_betas=1,
    #         # distance_metric=eval_utils.total_variation_prob_distance,
    #     )
    # ]
    # variations_tv = [
    #     eval_utils.model_variation(
    #         embeddings=train_embed.detach().numpy(),
    #         tasks_labels=Y_train[["ClassCategory_0"]].values,
    #         domains_labels=Y_train[["Machine"]].values,
    #         n_betas=1,
    #         distance_metric=eval_utils.total_variation_prob_distance,
    #     )
    # ]
    variations_kl = [
        eval_utils.model_variation(
            embeddings=train_embed.detach().numpy(),
            tasks_labels=Y_train[["ClassCategory_0"]].values,
            domains_labels=Y_train[["Machine"]].values,
            n_betas=n_betas,
            distance_metric=eval_utils.jensen_shannon_divergence_1d,
            random_state=random_seed,
        )
    ]
    # variations_mmd = [
    #     eval_utils.model_variation(
    #         embeddings=train_embed.detach().numpy(),
    #         tasks_labels=Y_train[["ClassCategory_0"]].values,
    #         domains_labels=Y_train[["Machine"]].values,
    #         n_betas=1,
    #         distance_metric=eval_utils.maximum_mean_discrepancy,
    #     )
    # ]
    # variations_icp = [0.0]
    scores = [
        train_embed_classer.score(
            train_embed.detach().numpy(), Y_train["ClassCategory_0"].values
        )
    ]
    print("Getting all other variations...")
    results = Parallel(n_jobs=-1, verbose=100)(
        delayed(
            lambda highest_variation_label: for_step(
                selfdis_ae_model,
                train_embed_classer,
                highest_variation_label,
                highest_train_label,
            )
        )(highest_variation_label)
        for highest_variation_label in np.sort(data.Y["Machine"].unique())[
            (highest_train_label + 1) :
        ]
    )

    # print("getting ICP variations...")
    # current_embed = selfdis_ae_model(torch.Tensor(data.X.values)).detach().numpy()
    # for highest_variation_label in np.sort(data.Y["Machine"].unique())[
    #     (highest_train_label + 1) :
    # ]:
    #     variations_icp.append(
    #         eval_utils.invariant_causal_prediction(
    #             embeddings=current_embed,
    #             task_labels=data.Y["ClassCategory_0"].values,
    #             domain_labels=data.Y["Machine"].values,
    #             source_labels=[i for i in range(highest_train_label + 1)],
    #             target_label=highest_variation_label,
    #         )
    #     )

    for result in results:
        machine_indices.append(result[0])
        scores.append(result[1])
        # variations_wasser.append(result[2])
        # variations_tv.append(result[3])
        variations_kl.append(result[2])
        # variations_mmd.append(result[5])

    return np.stack(
        (
            machine_indices,
            scores,
            # variations_wasser,
            # variations_tv,
            variations_kl,
            # variations_mmd,
            # variations_icp,
        ),
        axis=-1,
    )


# train on 1, 5, 10, 15, 20, 25 and save each result
for highest_train_label in [2, 3, 4, 6, 7, 8, 9, 12, 18, 22]:
    np.save(
        f"results_manyaffines/{highest_train_label+1}_machines_DisAE_variations_score_fixedrandom.npy",
        train_DisAE_and_get_variations(highest_train_label),
    )
