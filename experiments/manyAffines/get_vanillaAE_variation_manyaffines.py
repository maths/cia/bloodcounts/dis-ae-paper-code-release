import sys

sys.path.append("../../")

import numpy as np
import xgboost as xgb
import torch
from sklearn.model_selection import ShuffleSplit
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
import pytorch_lightning as pl

from joblib import Parallel, delayed

from disentanglement import models, eval_utils, training_utils


batch_size = 1024
n_betas = 1
random_seed = 12345

data = training_utils.Dataset(
    data="../../datasets/data_manyaffines.csv",
    X_labels=[str(i) for i in range(32)],
    Y_labels=[
        "ClassCategory_0",
        "Machine",
    ],
)

data.normalise()


def for_step(model, xgbclasser, highest_variation_label, highest_train_label):
    print(f"Getting variation for {highest_variation_label}...")
    # get variation
    x, y = data.get_filtered_slice(
        label="Machine", label_filter=[i for i in range(highest_variation_label)]
    )
    with torch.no_grad():
        current_embed = model(torch.Tensor(x.values)).detach().numpy()

    variation_kl = eval_utils.model_variation(
        embeddings=current_embed,
        tasks_labels=y[["ClassCategory_0"]].values,
        domains_labels=y[["Machine"]].values,
        n_betas=n_betas,
        distance_metric=eval_utils.jensen_shannon_divergence_1d,
        random_state=random_seed,
    )

    # get score
    x, y = data.get_filtered_slice(
        label="Machine",
        label_filter=[highest_variation_label],
    )
    with torch.no_grad():
        current_embed = model(torch.Tensor(x.values)).detach().numpy()
    score = xgbclasser.score(current_embed, y["ClassCategory_0"].values)
    return (
        highest_variation_label,
        score,
        # variation_wasser,
        # variation_tv,
        variation_kl,
        # variation_mmd,
    )


def train_DisAE_and_get_variations(highest_train_label):
    # get train data
    print(f"Training on {highest_train_label+1} machines...")
    print("Getting train data...")
    X_train, Y_train = data.get_filtered_slice(
        label="Machine",
        label_filter=[i for i in range(highest_train_label + 1)],
    )
    # make & train model
    model = models.LitAE(
        kernels=[16, 12],
        input_dim=32,
        overall_act="ReLU",
        latent_act="ReLU",
        output_act="None",
        scheduler_patience=20,
        lr=0.01,
    )
    print("Training model...")
    # split data
    rs = ShuffleSplit(n_splits=2, test_size=0.2, random_state=12345)
    for i, j in rs.split(X_train):
        x_train, y_train = X_train.iloc[i, :], Y_train.iloc[i, :]
        x_val, y_val = X_train.iloc[j, :], Y_train.iloc[j, :]

    # make dataloaders
    train_loader = training_utils.create_dataloader(
        x_train, y_train, batch_size=batch_size  # , num_dataloader_workers=2
    )
    val_loader = training_utils.create_dataloader(
        x_val, y_val, batch_size=batch_size, shuffle=False  # , num_dataloader_workers=2
    )
    # train model
    early_stop_callback = EarlyStopping(
        monitor="val_loss",
        min_delta=0.00,
        patience=batch_size / 4,
        verbose=False,
        mode="min",
    )
    trainer = pl.Trainer(
        max_epochs=1000,
        #     accelerator="mps",
        #     devices=8,
        callbacks=[early_stop_callback],
        enable_progress_bar=False,
    )
    trainer.fit(model=model, train_dataloaders=train_loader, val_dataloaders=val_loader)
    # get embeddings and variations
    print("Getting first variation...")
    model.eval()
    with torch.no_grad():
        train_embed = model(torch.Tensor(X_train.values))
    # train a new XGB classer on source
    train_embed_classer = xgb.XGBClassifier(use_label_encoder=False, verbosity=0).fit(
        train_embed.detach().numpy(), Y_train["ClassCategory_0"].values
    )
    machine_indices = [highest_train_label]

    variations_kl = [
        eval_utils.model_variation(
            embeddings=train_embed.detach().numpy(),
            tasks_labels=Y_train[["ClassCategory_0"]].values,
            domains_labels=Y_train[["Machine"]].values,
            n_betas=n_betas,
            distance_metric=eval_utils.jensen_shannon_divergence_1d,
            random_state=random_seed,
        )
    ]

    scores = [
        train_embed_classer.score(
            train_embed.detach().numpy(), Y_train["ClassCategory_0"].values
        )
    ]
    print("Getting all other variations...")
    results = Parallel(n_jobs=-1, verbose=100)(
        delayed(
            lambda highest_variation_label: for_step(
                model,
                train_embed_classer,
                highest_variation_label,
                highest_train_label,
            )
        )(highest_variation_label)
        for highest_variation_label in np.sort(data.Y["Machine"].unique())[
            (highest_train_label + 1) :
        ]
    )

    for result in results:
        machine_indices.append(result[0])
        scores.append(result[1])
        # variations_wasser.append(result[2])
        # variations_tv.append(result[3])
        variations_kl.append(result[2])
        # variations_mmd.append(result[5])

    return np.stack(
        (
            machine_indices,
            scores,
            # variations_wasser,
            # variations_tv,
            variations_kl,
            # variations_mmd,
            # variations_icp,
        ),
        axis=-1,
    )


# train on 1, 5, 10, 15, 20, 25 and save each result
for highest_train_label in [1, 5, 10, 15, 20, 25]:
    np.save(
        f"results_manyaffines/{highest_train_label+1}_machines_VanillaAE_variations_score_fixedrandom.npy",
        train_DisAE_and_get_variations(highest_train_label),
    )
