import sys

sys.path.append("../../")

import random
import copy

import matplotlib.pyplot as plt
import numpy as np
import ot
import pandas as pd
import phate
import pytorch_lightning as pl
import seaborn as sns
import torch
import torch.nn.functional as F
import xgboost as xgb
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
from sklearn.model_selection import KFold
from sklearn.preprocessing import LabelEncoder

from disentanglement import models
from disentanglement.eval_utils import (
    fast_jensen_shannon_divergence_1d,
    make_PHATE_plot,
    model_selection_score,
    score_per_class,
    variation_different_targets,
)
from disentanglement.training_utils import Dataset
from disentanglement.utils import multiencode

sns.set_theme()
sns.set(rc={"figure.dpi": 150})
sns.set_style("whitegrid", {"legend.frameon": True, "grid.linestyle": "--"})

# ## Load data
feats_hl = [
    "PLT#",
    "MPV",
    #     "PDW",
    "PCT",
    "RBC#",
    "MCV",
    "HCT",
    "MCH",
    "MCHC",
    "HGB",
    "RDW",
    "MONO#",
    "NEUT#",
    "EO#",
    "BASO#",
    #     "(NEUT+EO)#",
    #     "(EO+BASO)#",
    #     "(BASO+NEUT)#",
    #     "GRAN#",
    #     "NEUT%GRAN",
    #     "BASO%GRAN",
    #     "EO%GRAN",
    #     "MYELOID#",
    #     "GRAN%MYELOID",
    "LYMPH#",
    "WBC#",
    "MONO%",
    "NEUT%",
    "EO%",
    "BASO%",
    "LYMPH%",
]

print("Loading data...")
data = Dataset(
    data="/epicov/data/fbc_data/BloodCounts_processed/Kreuter_Daniel_DisAE_EpiCov/HL-FBC_EpiCov_withoutMac5.csv",
    X_labels=feats_hl,
    Y_labels=[
        "Sex",
        #         "Ethnicity",
        "Analyser ID",
        #         "d_week",
        #         "t_day",
        "vendelay",
        "Season",
        "age_bracket",
        #         "bmi_bracket",
        #         "BMI",
        "Age",
        "Test Submitter",
    ],
)


data.normalise()

tasks = ["Sex", "age_bracket", "Test Submitter"]
domains = ["Analyser ID", "vendelay", "Season"]
batch_size = 2048

# Set seeds for reproducibility
torch.manual_seed(42)
np.random.seed(42)
random.seed(42)


data.normalise()

data.X.reset_index(inplace=True, drop=True)
data.Y.reset_index(inplace=True, drop=True)

data.Y, Y_encoder = multiencode(data.Y)

# Create file for encoded label metadata
file_path = "y_encoded_labels.txt"

with open(file_path, "w") as file:
    file.write("Encoded Y labels:\n")
    for encoder in Y_encoder.encoders:
        if encoder != 0:
            file.write("----------------------------------------\n")
            for i, class_name in enumerate(encoder.classes_):
                file.write(f"Label {i}: {class_name}\n")
            file.write("----------------------------------------\n")


# find machine pair with maximum variation as source
import itertools

machine_pairs_product = list(
    itertools.product(data.Y["Analyser ID"].unique(), data.Y["Analyser ID"].unique())
)

machine_pairs = []
for tup in machine_pairs_product:
    if len(set(tup)) != 1:
        machine_pairs.append(tup)

max_variation = -np.inf
max_index = ""
max_pair = ""
for pair in machine_pairs:
    first_machine_data = data.X[data.Y["Analyser ID"] == pair[0]]
    second_machine_data = data.X[data.Y["Analyser ID"] == pair[1]]
    for index in feats_hl:
        variation = ot.emd2_1d(
            first_machine_data[index].values,
            second_machine_data[index].values,
        )
        if variation > max_variation:
            max_variation = variation
            max_index = index
            max_pair = list(pair)
print(
    f"The maximum variation is on the machine pair {max_pair}. The wasserstein distance is {max_variation:.3f} on {max_index}. Using that pair as source."
)

data_source_target = copy.deepcopy(data)
data_source_target.Y["Analyser ID"] = data_source_target.Y["Analyser ID"].apply(
    lambda x: 100 if x != max_pair[0] and x != max_pair[1] else x
)

X_source, Y_source = data.get_filtered_slice(
    label="Analyser ID", label_filter=[max_pair[0], max_pair[1]]
)

# all machines that aren't source (that aren't in max_pair)
target_machines = [
    i for i in np.sort(data.Y["Analyser ID"].unique()) if i not in max_pair
]
data_targets = [
    data.get_filtered_slice(label="Analyser ID", label_filter=target_machines)
]

#################################
# HIGH-DIM DATA PHATE PLOTS
#################################
sample_X = data.X.sample(n=100_000, random_state=42)
sample_Y = data.Y.loc[sample_X.index]
print("Making PHATE plots using data of shape: ", sample_X.shape)

phate_operator_raw = phate.PHATE(n_jobs=-1, knn=20)
raw_phate = phate_operator_raw.fit_transform(sample_X.values)
g = make_PHATE_plot(
    hue_var="Test Submitter",
    y_data=sample_Y,
    encoder_list=Y_encoder.encoders,
    phate_array=raw_phate,
    subsample_size=2000,
)

# plt.xlabel("PHATE1")
# plt.ylabel("PHATE2")
plt.xticks([])
plt.yticks([])
# plt.title("Scatterplot of PHATE-embedded latent space")

# these are matplotlib.patch.Patch properties
props = dict(boxstyle="round", facecolor="white", alpha=1.0, edgecolor="black")

# place a text box in upper left in axes coords
g.axes.text(
    0.02,
    0.07,
    "Normalised Data",
    transform=g.axes.transAxes,
    fontsize=14,
    verticalalignment="top",
    bbox=props,
)

plt.legend(title="Care Setting", loc="lower right")

plt.tight_layout()

plt.savefig("EpiCov_raw_PHATE_caresetting.png")
plt.close()

g = make_PHATE_plot(
    hue_var="Analyser ID",
    y_data=sample_Y,
    encoder_list=Y_encoder.encoders,
    phate_array=raw_phate,
    hue_order=[
        "ADVIA 2120-1",
        "ADVIA 2120-2",
        "ADVIA 2120-3",
        "ADVIA 2120-4",
        "ADVIA 2120I 1",
        "ADVIA 2120I 2",
        "ADVIA 2120I 3",
        "ADVIA 2120I 4",
    ],
    subsample_size=2000,
)

# plt.xlabel("PHATE1")
# plt.ylabel("PHATE2")
plt.xticks([])
plt.yticks([])
# plt.title("Scatterplot of PHATE-embedded latent space")

# these are matplotlib.patch.Patch properties
props = dict(boxstyle="round", facecolor="white", alpha=1.0, edgecolor="black")

# place a text box in upper left in axes coords
g.axes.text(
    0.02,
    0.07,
    "Normalised Data",
    transform=g.axes.transAxes,
    fontsize=14,
    verticalalignment="top",
    bbox=props,
)

plt.legend(
    title="Analyser",
    loc="lower right",
)

plt.tight_layout()

plt.savefig("EpiCov_raw_PHATE_machine.png")
plt.close()

#################################

#################################
# HIGH-DIM DATA VARIATIONS
#################################
print("Calculating high-dim data variations...")
variations = np.zeros((len(domains), len(data_targets) + 1))
for i, domain in enumerate(domains):
    variations[i] = variation_different_targets(
        dataset=data_source_target,
        domains_labels=[domain],
        tasks_labels=tasks,
        target_label="Analyser ID",
        n_betas=1000,
        source_labels=list(Y_source["Analyser ID"].unique()),
        distance_metric=fast_jensen_shannon_divergence_1d,
    )

# turn into dataframe
variations_df = pd.DataFrame(
    variations,
    index=domains,
    columns=["MAX PAIR", "MAX PAIR + ALL OTHERS"],
)
# save CSV
variations_df.to_csv("highdim_variations.csv")

# do 5-fold cross-validation on normalised data
results = []
kf = KFold(n_splits=5, shuffle=True)
for fold_idx, (train_idx, test_idx) in enumerate(kf.split(X_source), 1):
    print(f"Fold {fold_idx}")
    # split data
    x_train, x_val = X_source.iloc[train_idx, :], X_source.iloc[test_idx, :]
    y_train, y_val = Y_source.iloc[train_idx, :], Y_source.iloc[test_idx, :]

    # get per-class predictions
    # add the predictions on source data
    task_models = []
    for task in tasks:
        best_model = xgb.XGBClassifier(n_jobs=-1).fit(x_train, y_train[task])
        task_models.append(best_model)
        scores = score_per_class(best_model, x_val.values, y_val[task].values)
        for score_idx, score in enumerate(scores):
            results.append(
                {
                    "fold": fold_idx,
                    "task": task,
                    "task_label": score_idx,
                    "accuracy": score,
                    # "data": "normalised",
                    "type": "source",
                }
            )
    # add for all target datasets
    for target_idx, target in enumerate(data_targets):
        for task_idx, task in enumerate(tasks):
            scores = score_per_class(
                task_models[task_idx], target[0].values, target[1][task].values
            )
            for score_idx, score in enumerate(scores):
                results.append(
                    {
                        "fold": fold_idx,
                        "task": task,
                        "task_label": score_idx,
                        "accuracy": score,
                        # "data": "h",
                        "type": "target",
                        "target": target_idx,
                    }
                )

    # add machine predictin on source as extra column
    # for XGBClassifier to work, need to encode labels to 0,1
    label_encoder = LabelEncoder().fit(y_train["Analyser ID"].values)
    train_labels = label_encoder.transform(y_train["Analyser ID"].values)
    val_labels = label_encoder.transform(y_val["Analyser ID"].values)
    machine_pred = (
        xgb.XGBClassifier()
        .fit(x_train.values, train_labels)
        .score(x_val.values, val_labels)
    )
    results.append(
        {
            "fold": fold_idx,
            "task": "Source Machine Prediction",
            # "task_label": 0,
            "accuracy": machine_pred,
            # "data": "normalised",
            "type": "source",
        }
    )

raw_crossval_df = pd.DataFrame(results)
raw_crossval_df.to_csv("highdim_crossval.csv")


###############################################################
# SAME AGAIN FOR AUTOENCODER
###############################################################
print("Starting autoencoder...")
results = []
ae_models = []
trainers = []
kf = KFold(n_splits=5, shuffle=True)
for fold_idx, (train_idx, test_idx) in enumerate(kf.split(X_source), 1):
    print(f"Fold {fold_idx}")
    # split data
    x_train, x_val = X_source.iloc[train_idx, :], X_source.iloc[test_idx, :]
    y_train, y_val = Y_source.iloc[train_idx, :], Y_source.iloc[test_idx, :]
    # make dataloaders
    train_loader = models.dataloader(
        x_train, y_train, batch_size=batch_size, balance_on="Test Submitter"
    )
    val_loader = models.dataloader(x_val, y_val, batch_size=batch_size)

    # train 5 times and keep the best model
    best_model = None
    for i in range(5):
        early_stop_callback = EarlyStopping(
            monitor="val_loss",
            min_delta=0.00,
            patience=batch_size / 8,
            verbose=False,
            mode="min",
        )
        trainer = pl.Trainer(
            max_epochs=10_000,
            # accelerator="cpu",
            # devices=32,
            callbacks=[early_stop_callback],
            enable_progress_bar=False,
            enable_checkpointing=False,
        )
        model = models.LitAE(
            kernels=[32, 16],
            input_dim=x_train.shape[1],
            overall_act="ReLU",
            latent_act="ReLU",
            output_act="None",
            scheduler_patience=20,
            lr=0.01,
        )
        print(f"Training AE model for fold {fold_idx} and run {i+1}...")
        trainer.fit(
            model=model, train_dataloaders=train_loader, val_dataloaders=val_loader
        )
        # model.best_loss = trainer.callback_metrics["val_loss"].min()
        model.eval()
        model.best_loss = F.mse_loss(
            model._run_step(torch.Tensor(X_source.values)),
            torch.Tensor(X_source.values),
        ).item()

        print("Current model loss:", model.best_loss)

        if best_model is None:
            best_model = model
            best_trainer = trainer
        elif model.best_loss < best_model.best_loss:
            best_model = model
            best_trainer = trainer

    ae_models.append(best_model)
    trainers.append(best_trainer)

    best_model.eval()
    with torch.no_grad():
        source_embed = best_model(torch.Tensor(X_source.values))
        train_embed = best_model(torch.Tensor(x_train.values))
        val_embed = best_model(torch.Tensor(x_val.values))
        target_embeds = []
        for target in data_targets:
            target_embeds.append(best_model(torch.Tensor(target[0].values)))

    # selection score on source data
    with torch.no_grad():
        reconstructions = best_model._run_step(torch.Tensor(X_source.values)).detach()

    # # for debugging, save some plots of original vs reconstruction
    # if fold_idx == 1:
    #     for i in range(10):
    #         x = np.arange(0, X_source.shape[1])
    #         plt.plot(x, X_source.values[i], label="original")
    #         plt.plot(x, reconstructions[i], label="reconstruction")
    #         plt.legend()
    #         plt.savefig(f"reconstructions_{i}.png")
    #         plt.close()

    print("Calculating AE model selection score...")
    ae_score, ae_accuracy, ae_variation, ae_reconstruction = model_selection_score(
        originals=X_source.values,
        reconstructions=reconstructions,
        embeddings=source_embed.detach().numpy(),
        tasks_labels=Y_source[tasks].values,
        domains_labels=Y_source[domains].values,
        distance_metric=ot.emd2_1d,
        n_betas=1000,
    )
    results.append(
        {
            "fold": fold_idx,
            "selection_score": ae_score,
            "selection_accuracy": ae_accuracy,
            "selection_variation": ae_variation,
            "selection_reconstruction": ae_reconstruction,
            "type": "source",
        }
    )

    print("Calculating AE per-class accuracies...")
    # get per-class predictions
    # add the predictions on source data
    task_models = []
    for task in tasks:
        classer = xgb.XGBClassifier(n_jobs=-1).fit(
            train_embed.detach().numpy(), y_train[task].values
        )
        task_models.append(classer)
        scores = score_per_class(
            classer, val_embed.detach().numpy(), y_val[task].values
        )
        for score_idx, score in enumerate(scores):
            results.append(
                {
                    "fold": fold_idx,
                    "task": task,
                    "task_label": score_idx,
                    "accuracy": score,
                    # "data": "normalised",
                    "type": "source",
                }
            )
    # add for all target datasets
    for target_idx, target in enumerate(target_embeds):
        for task_idx, task in enumerate(tasks):
            scores = score_per_class(
                task_models[task_idx],
                target.detach().numpy(),
                data_targets[target_idx][1][task].values,
            )
            for score_idx, score in enumerate(scores):
                results.append(
                    {
                        "fold": fold_idx,
                        "task": task,
                        "task_label": score_idx,
                        "accuracy": score,
                        # "data": "h",
                        "type": "target",
                        "target": target_idx,
                    }
                )

    print("Calculating AE machine prediction accuracy on source data...")
    # add machine predictin on source as extra column
    # for XGBClassifier to work, need to encode labels to 0,1
    label_encoder = LabelEncoder().fit(y_train["Analyser ID"].values)
    train_labels = label_encoder.transform(y_train["Analyser ID"].values)
    val_labels = label_encoder.transform(y_val["Analyser ID"].values)
    machine_pred = (
        xgb.XGBClassifier()
        .fit(train_embed.detach().numpy(), train_labels)
        .score(val_embed.detach().numpy(), val_labels)
    )
    results.append(
        {
            "fold": fold_idx,
            "task": "Source Machine Prediction",
            # "task_label": 0,
            "accuracy": machine_pred,
            # "data": "normalised",
            "type": "source",
        }
    )
    print("Calculating AE variation for source and source+target...")
    # add variations for AE latent space for all domains calculating for source and source+target
    for i, domain in enumerate(domains):
        with torch.no_grad():
            variation = variation_different_targets(
                dataset=data_source_target,
                domains_labels=[domain],
                tasks_labels=tasks,
                target_label="Analyser ID",
                n_betas=1000,
                source_labels=list(Y_source["Analyser ID"].unique()),
                distance_metric=fast_jensen_shannon_divergence_1d,
                model=best_model,
            )
        results.append(
            {
                "fold": fold_idx,
                "domain": domain,
                "variation": variation[0],
                "type": "source",
            }
        )
        for target_idx, _ in enumerate(data_targets):
            results.append(
                {
                    "fold": fold_idx,
                    "domain": domain,
                    "variation": variation[target_idx + 1],
                    "type": "target",
                    "target": target_idx,
                }
            )

ae_crossval_df = pd.DataFrame(results)
ae_crossval_df.to_csv("ae_crossval.csv", index=False)

#################################
# AE DATA PHATE PLOTS
#################################
sample_X = data.X.sample(n=100_000, random_state=42)
sample_Y = data.Y.loc[sample_X.index]

# get best AE model (lowest loss)
best_model = None
for i, model in enumerate(ae_models):
    if best_model is None:
        best_model = model
        best_trainer = trainers[i]
    elif model.best_loss < best_model.best_loss:
        best_model = model
        best_trainer = trainers[i]

best_trainer.save_checkpoint("best_ae_model.ckpt")

best_model.eval()
with torch.no_grad():
    all_embed = best_model(torch.Tensor(sample_X.values))

print("Making PHATE plots using data of shape: ", all_embed.shape)

phate_operator_raw = phate.PHATE(n_jobs=-1)
ae_phate = phate_operator_raw.fit_transform(all_embed.detach().numpy())
g = make_PHATE_plot(
    hue_var="Test Submitter",
    y_data=sample_Y,
    encoder_list=Y_encoder.encoders,
    phate_array=ae_phate,
    subsample_size=2000,
)

# plt.xlabel("PHATE1")
# plt.ylabel("PHATE2")
plt.xticks([])
plt.yticks([])
# plt.title("Scatterplot of PHATE-embedded latent space")

# these are matplotlib.patch.Patch properties
props = dict(boxstyle="round", facecolor="white", alpha=1.0, edgecolor="black")

# place a text box in upper left in axes coords
g.axes.text(
    0.02,
    0.07,
    "Vanilla AE Latent Space",
    transform=g.axes.transAxes,
    fontsize=14,
    verticalalignment="top",
    bbox=props,
)

plt.legend(title="Care Setting", loc="lower right")

plt.tight_layout()

plt.savefig("EpiCov_AE_PHATE_caresetting.png")
plt.close()

g = make_PHATE_plot(
    hue_var="Analyser ID",
    y_data=sample_Y,
    encoder_list=Y_encoder.encoders,
    phate_array=ae_phate,
    hue_order=[
        "ADVIA 2120-1",
        "ADVIA 2120-2",
        "ADVIA 2120-3",
        "ADVIA 2120-4",
        "ADVIA 2120I 1",
        "ADVIA 2120I 2",
        "ADVIA 2120I 3",
        "ADVIA 2120I 4",
    ],
    subsample_size=2000,
)

# plt.xlabel("PHATE1")
# plt.ylabel("PHATE2")
plt.xticks([])
plt.yticks([])
# plt.title("Scatterplot of PHATE-embedded latent space")

# these are matplotlib.patch.Patch properties
props = dict(boxstyle="round", facecolor="white", alpha=1.0, edgecolor="black")

# place a text box in upper left in axes coords
g.axes.text(
    0.02,
    0.07,
    "Vanilla AE Latent Space",
    transform=g.axes.transAxes,
    fontsize=14,
    verticalalignment="top",
    bbox=props,
)

plt.legend(
    title="Analyser",
    loc="lower right",
)

plt.tight_layout()

plt.savefig("EpiCov_AE_PHATE_machine.png")
plt.close()

#################################

###############################################################
# SAME AGAIN FOR Dis-AE
###############################################################
print("Starting Dis-AE...")
results = []
disae_models = []
kf = KFold(n_splits=5, shuffle=True)
for fold_idx, (train_idx, test_idx) in enumerate(kf.split(X_source), 1):
    print(f"Fold {fold_idx}")
    # split data
    x_train, x_val = X_source.iloc[train_idx, :], X_source.iloc[test_idx, :]
    y_train, y_val = Y_source.iloc[train_idx, :], Y_source.iloc[test_idx, :]
    # concat for Dis-AE format
    train_data = pd.concat([x_train, y_train], axis=1)
    val_data = pd.concat([x_val, y_val], axis=1)

    # train 5 times and keep the best model
    best_model = None
    for i in range(5):
        selfdis_ae_model = models.Sam_DisAE(
            heads=[
                ["Sex", True, True],
                ["Analyser ID", False, True],
                ["vendelay", False, True],
                ["Season", False, True],
                ["age_bracket", True, True],
                ["Test Submitter", True, True],
            ],
            layers=[21, 32, 16],
            reconstruction_factor=9.13,
            task_factor=2.505,
            domain_factor=3.505,
        )
        print(f"Training Dis-AE model for fold {fold_idx} and run {i+1}...")
        selfdis_ae_model.fit(
            data=train_data,
            X_columns=feats_hl,
            batch_size=batch_size,
            weight_decay=0.01,
            lr_0=0.001,
            #     patience=batch_size / 16,
            balance_on="Test Submitter",
            save_model=False,
        )
        # selection score on source data
        with torch.no_grad():
            source_embed = selfdis_ae_model(torch.Tensor(X_source.values))
            reconstructions = selfdis_ae_model.decoder(source_embed).detach()

        # print("Calculating Dis-AE model selection score...")
        selfdis_ae_model.score, _, _, _ = model_selection_score(
            originals=X_source.values,
            reconstructions=reconstructions,
            embeddings=source_embed.detach().numpy(),
            tasks_labels=Y_source[tasks].values,
            domains_labels=Y_source[domains].values,
            distance_metric=ot.emd2_1d,
            n_betas=1000,
        )
        if best_model is None:
            best_model = selfdis_ae_model
        elif selfdis_ae_model.score > best_model.score:
            best_model = selfdis_ae_model

    disae_models.append(best_model)

    with torch.no_grad():
        source_embed = best_model(torch.Tensor(X_source.values))
        train_embed = best_model(torch.Tensor(x_train.values))
        val_embed = best_model(torch.Tensor(x_val.values))
        target_embeds = []
        for target in data_targets:
            target_embeds.append(best_model(torch.Tensor(target[0].values)))

    # selection score on source data
    with torch.no_grad():
        reconstructions = best_model.decoder(source_embed).detach()

    print("Calculating Dis-AE model selection score...")
    ae_score, ae_accuracy, ae_variation, ae_reconstruction = model_selection_score(
        originals=X_source.values,
        reconstructions=reconstructions,
        embeddings=source_embed.detach().numpy(),
        tasks_labels=Y_source[tasks].values,
        domains_labels=Y_source[domains].values,
        distance_metric=ot.emd2_1d,
        n_betas=1000,
    )
    results.append(
        {
            "fold": fold_idx,
            "selection_score": ae_score,
            "selection_accuracy": ae_accuracy,
            "selection_variation": ae_variation,
            "selection_reconstruction": ae_reconstruction,
            "type": "source",
        }
    )

    print("Calculating Dis-AE per-class accuracies...")
    # get per-class predictions
    # add the predictions on source data
    task_models = []
    for task in tasks:
        classer = xgb.XGBClassifier(n_jobs=-1).fit(
            train_embed.detach().numpy(), y_train[task].values
        )
        task_models.append(classer)
        scores = score_per_class(
            classer, val_embed.detach().numpy(), y_val[task].values
        )
        for score_idx, score in enumerate(scores):
            results.append(
                {
                    "fold": fold_idx,
                    "task": task,
                    "task_label": score_idx,
                    "accuracy": score,
                    # "data": "normalised",
                    "type": "source",
                }
            )
    # add for all target datasets
    for target_idx, target in enumerate(target_embeds):
        for task_idx, task in enumerate(tasks):
            scores = score_per_class(
                task_models[task_idx],
                target.detach().numpy(),
                data_targets[target_idx][1][task].values,
            )
            for score_idx, score in enumerate(scores):
                results.append(
                    {
                        "fold": fold_idx,
                        "task": task,
                        "task_label": score_idx,
                        "accuracy": score,
                        # "data": "h",
                        "type": "target",
                        "target": target_idx,
                    }
                )

    print("Calculating Dis-AE machine prediction accuracy on source data...")
    # add machine predictin on source as extra column
    # for XGBClassifier to work, need to encode labels to 0,1
    label_encoder = LabelEncoder().fit(y_train["Analyser ID"].values)
    train_labels = label_encoder.transform(y_train["Analyser ID"].values)
    val_labels = label_encoder.transform(y_val["Analyser ID"].values)
    machine_pred = (
        xgb.XGBClassifier()
        .fit(train_embed.detach().numpy(), train_labels)
        .score(val_embed.detach().numpy(), val_labels)
    )
    results.append(
        {
            "fold": fold_idx,
            "task": "Source Machine Prediction",
            # "task_label": 0,
            "accuracy": machine_pred,
            # "data": "normalised",
            "type": "source",
        }
    )
    print("Calculating Dis-AE variation for source and source+target...")
    # add variations for AE latent space for all domains calculating for source and source+target
    for i, domain in enumerate(domains):
        with torch.no_grad():
            variation = variation_different_targets(
                dataset=data_source_target,
                domains_labels=[domain],
                tasks_labels=tasks,
                target_label="Analyser ID",
                n_betas=1000,
                source_labels=list(Y_source["Analyser ID"].unique()),
                distance_metric=fast_jensen_shannon_divergence_1d,
                model=best_model,
            )
        results.append(
            {
                "fold": fold_idx,
                "domain": domain,
                "variation": variation[0],
                "type": "source",
            }
        )
        for target_idx, _ in enumerate(data_targets):
            results.append(
                {
                    "fold": fold_idx,
                    "domain": domain,
                    "variation": variation[target_idx + 1],
                    "type": "target",
                    "target": target_idx,
                }
            )

disae_crossval_df = pd.DataFrame(results)
disae_crossval_df.to_csv("disae_crossval.csv", index=False)

#################################
# Dis-AE DATA PHATE PLOTS
#################################
sample_X = data.X.sample(n=100_000, random_state=42)
sample_Y = data.Y.loc[sample_X.index]

# get best AE model (lowest loss)
best_model = None
for model in disae_models:
    if best_model is None:
        best_model = model
    elif model.score > best_model.score:
        best_model = model

best_model.save_model("best_disae_model")
# best_model.eval()
with torch.no_grad():
    all_embed = best_model(torch.Tensor(sample_X.values))

print("Making PHATE plots using data of shape: ", all_embed.shape)

phate_operator_raw = phate.PHATE(n_jobs=-1)
disae_phate = phate_operator_raw.fit_transform(all_embed.detach().numpy())
g = make_PHATE_plot(
    hue_var="Test Submitter",
    y_data=sample_Y,
    encoder_list=Y_encoder.encoders,
    phate_array=disae_phate,
    subsample_size=2000,
)

# plt.xlabel("PHATE1")
# plt.ylabel("PHATE2")
plt.xticks([])
plt.yticks([])
# plt.title("Scatterplot of PHATE-embedded latent space")

# these are matplotlib.patch.Patch properties
props = dict(boxstyle="round", facecolor="white", alpha=1.0, edgecolor="black")

# place a text box in upper left in axes coords
g.axes.text(
    0.02,
    0.07,
    "Dis-AE Latent Space",
    transform=g.axes.transAxes,
    fontsize=14,
    verticalalignment="top",
    bbox=props,
)

plt.legend(title="Care Setting", loc="lower right")

plt.tight_layout()

plt.savefig("EpiCov_Dis-AE_PHATE_caresetting.png")
plt.close()

g = make_PHATE_plot(
    hue_var="Analyser ID",
    y_data=sample_Y,
    encoder_list=Y_encoder.encoders,
    phate_array=disae_phate,
    hue_order=[
        "ADVIA 2120-1",
        "ADVIA 2120-2",
        "ADVIA 2120-3",
        "ADVIA 2120-4",
        "ADVIA 2120I 1",
        "ADVIA 2120I 2",
        "ADVIA 2120I 3",
        "ADVIA 2120I 4",
    ],
    subsample_size=2000,
)

# plt.xlabel("PHATE1")
# plt.ylabel("PHATE2")
plt.xticks([])
plt.yticks([])
# plt.title("Scatterplot of PHATE-embedded latent space")

# these are matplotlib.patch.Patch properties
props = dict(boxstyle="round", facecolor="white", alpha=1.0, edgecolor="black")

# place a text box in upper left in axes coords
g.axes.text(
    0.02,
    0.07,
    "Dis-AE Latent Space",
    transform=g.axes.transAxes,
    fontsize=14,
    verticalalignment="top",
    bbox=props,
)

plt.legend(
    title="Analyser",
    loc="lower right",
)

plt.tight_layout()

plt.savefig("EpiCov_Dis-AE_PHATE_machine.png")
plt.close()

#################################
print("... done. Script successful.")
