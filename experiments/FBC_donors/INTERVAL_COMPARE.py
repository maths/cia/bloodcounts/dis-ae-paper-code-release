import sys

sys.path.append("../../")

import random

import matplotlib.pyplot as plt
import numpy as np
import ot
import pandas as pd
import phate
import pytorch_lightning as pl
import seaborn as sns
import torch
import torch.nn.functional as F
import xgboost as xgb
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
from sklearn.model_selection import KFold

from disentanglement import models
from disentanglement.eval_utils import (
    fast_jensen_shannon_divergence_1d,
    make_PHATE_plot,
    model_selection_score,
    score_per_class,
    variation_different_targets,
)
from disentanglement.training_utils import Dataset
from disentanglement.utils import multiencode

sns.set_theme()
sns.set(rc={"figure.dpi": 150})
sns.set_style("whitegrid", {"legend.frameon": True, "grid.linestyle": "--"})

feats_bl = [
    "WBC_10_9_L_bl",
    "RBC_10_12_L_bl",
    "HGB_g_dL_bl",
    "HCT_PCT_bl",
    "MCV_fL_bl",
    "MCH_pg_bl",
    "MCHC_g_dL_bl",
    "PLT_10_9_L_bl",
    "RDW_SD_fL_bl",
    "RDW_CV_PCT_bl",
    "PDW_fL_bl",
    "MPV_fL_bl",
    "P_LCR_PCT_bl",
    "PCT_PCT_bl",
    #     "NRBC_10_9_L_bl",
    #     "NRBC_PCT_bl",
    "NEUT_10_9_L_bl",
    "LYMPH_10_9_L_bl",
    "MONO_10_9_L_bl",
    "EO_10_9_L_bl",
    "BASO_10_9_L_bl",
    "NEUT_PCT_bl",
    "LYMPH_PCT_bl",
    "MONO_PCT_bl",
    "EO_PCT_bl",
    "BASO_PCT_bl",
    "IG_10_9_L_bl",
    "IG_PCT_bl",
    "RET_PCT_bl",
    "RET_10_6_uL_bl",
    "IRF_PCT_bl",
    "LFR_PCT_bl",
    "MFR_PCT_bl",
    "HFR_PCT_bl",
    "RET_He_pg_bl",
    "IPF_bl",
    "PLT_I_10_9_L_bl",
    "MicroR_PCT_bl",
    "MacroR_PCT_bl",
    "TNC_10_9_L_bl",
    "WBC_N_10_9_L_bl",
    "TNC_N_10_9_L_bl",
    "BA_N_10_9_L_bl",
    "BA_N_PCT_bl",
    "WBC_D_10_9_L_bl",
    "TNC_D_10_9_L_bl",
    "NEUTx_10_9_L_bl",
    "NEUTx_PCT_bl",
    "LYMP_10_9_L_bl",
    "LYMP_PCT_bl",
    #     "HFLC_10_9_L_bl",
    #     "HFLC_PCT_bl",
    "BA_D_10_9_L_bl",
    "BA_D_PCT_bl",
    "NE_SSC_ch_bl",
    "NE_SFL_ch_bl",
    "NE_FSC_ch_bl",
    "LY_X_ch_bl",
    "LY_Y_ch_bl",
    "LY_Z_ch_bl",
    "MO_X_ch_bl",
    "MO_Y_ch_bl",
    "MO_Z_ch_bl",
    "NE_WX_bl",
    "NE_WY_bl",
    "NE_WZ_bl",
    "LY_WX_bl",
    "LY_WY_bl",
    "LY_WZ_bl",
    "MO_WX_bl",
    "MO_WY_bl",
    "MO_WZ_bl",
    "RBC_O_10_12_L_bl",
    "PLT_O_10_9_L_bl",
    "RBC_He_pg_bl",
    "Delta_He_pg_bl",
    "RET_Y_ch_bl",
    "RET_RBC_Y_ch_bl",
    "IRF_Y_ch_bl",
    #     "FRC_10_12_L_bl",
    #     "FRC_PCT_bl",
    "HYPO_He_PCT_bl",
    "HYPER_He_PCT_bl",
    "RPI_bl",
    "RET_UPP_bl",
    "RET_TNC_bl",
    "PLT_F_10_9_L_bl",
    "H_IPF_bl",
    "IPFx_10_9_L_bl",
]
feats_rich = []
for feature in feats_bl:
    if feature[-2:] == "bl":
        feats_rich.append(feature[:-3])

print("Loading data...")
data = Dataset(
    data="../../datasets/FBC_donordata.csv",
    X_labels=feats_rich,
    Y_labels=[
        "Sex",
        "Ethnicity",
        "Analyser ID",
        "d_week",
        "t_day",
        "vendelay",
        "season",
        "age_bracket",
        "bmi_bracket",
        "BMI",
        "Age",
    ],
)

tasks = ["Sex", "age_bracket", "bmi_bracket"]
domains = ["Analyser ID", "vendelay", "season"]
batch_size = 1024

# Set seeds for reproducibility
torch.manual_seed(42)
np.random.seed(42)
random.seed(42)


data.normalise()

data.X.reset_index(inplace=True, drop=True)
data.Y.reset_index(inplace=True, drop=True)

data.Y, Y_encoder = multiencode(data.Y)

# Create file for encoded label metadata
file_path = "y_encoded_labels.txt"

with open(file_path, "w") as file:
    file.write("Encoded Y labels:\n")
    for encoder in Y_encoder.encoders:
        if encoder != 0:
            file.write("----------------------------------------\n")
            for i, class_name in enumerate(encoder.classes_):
                file.write(f"Label {i}: {class_name}\n")
            file.write("----------------------------------------\n")


# source data are the first two analysers
X_source, Y_source = data.get_filtered_slice(label="Analyser ID", label_filter=[0, 1])

# target data are the rest
data_targets = []
for i in np.sort(data.Y["Analyser ID"].unique())[2:]:
    data_targets.append(data.get_filtered_slice(label="Analyser ID", label_filter=[i]))


#################################
# HIGH-DIM DATA PHATE PLOTS
#################################
# sample_X = data.X.sample(n=100_000, random_state=42)
# sample_Y = data.Y.loc[sample_X.index]
print("Making PHATE plots using data of shape: ", data.X.shape)

phate_operator_raw = phate.PHATE(n_jobs=-1)
raw_phate = phate_operator_raw.fit_transform(data.X.values)
g = make_PHATE_plot(
    hue_var="Sex",
    y_data=data.Y,
    encoder_list=Y_encoder.encoders,
    phate_array=raw_phate,
)

# plt.xlabel("PHATE1")
# plt.ylabel("PHATE2")
plt.xticks([])
plt.yticks([])
# plt.title("Scatterplot of PHATE-embedded latent space")

# these are matplotlib.patch.Patch properties
props = dict(boxstyle="round", facecolor="white", alpha=1.0, edgecolor="black")

# place a text box in upper left in axes coords
g.axes.text(
    0.02,
    0.07,
    "Normalised Data",
    transform=g.axes.transAxes,
    fontsize=14,
    verticalalignment="top",
    bbox=props,
)

plt.legend(title="Sex", loc="lower right")

plt.tight_layout()

plt.savefig("FBC_raw_PHATE_sex.png")
plt.close()

g = make_PHATE_plot(
    hue_var="Analyser ID",
    y_data=data.Y,
    encoder_list=Y_encoder.encoders,
    phate_array=raw_phate,
    hue_order=["INTERVAL 1", "INTERVAL 2", "COMPARE"],
)

# plt.xlabel("PHATE1")
# plt.ylabel("PHATE2")
plt.xticks([])
plt.yticks([])
# plt.title("Scatterplot of PHATE-embedded latent space")

# these are matplotlib.patch.Patch properties
props = dict(boxstyle="round", facecolor="white", alpha=1.0, edgecolor="black")

# place a text box in upper left in axes coords
g.axes.text(
    0.02,
    0.07,
    "Normalised Data",
    transform=g.axes.transAxes,
    fontsize=14,
    verticalalignment="top",
    bbox=props,
)

plt.legend(
    title="Analyser",
    loc="lower right",
    #    labels=["INTERVAL 1", "INTERVAL 2", "COMPARE"]
)

plt.tight_layout()

plt.savefig("FBC_raw_PHATE_machine.png")
plt.close()

#################################

#################################
# HIGH-DIM DATA VARIATIONS
#################################
print("Calculating high-dim data variations...")
variations = np.zeros((len(domains), len(data_targets) + 1))
for i, domain in enumerate(domains):
    variations[i] = variation_different_targets(
        dataset=data,
        domains_labels=[domain],
        tasks_labels=tasks,
        target_label="Analyser ID",
        n_betas=1000,
        source_labels=list(Y_source["Analyser ID"].unique()),
        distance_metric=fast_jensen_shannon_divergence_1d,
    )

# turn into dataframe
variations_df = pd.DataFrame(
    variations,
    index=domains,
    columns=["INTERVAL", "INTERVAL + COMPARE"],
)
# save CSV
variations_df.to_csv("highdim_variations.csv")

# do 5-fold cross-validation on normalised data
results = []
kf = KFold(n_splits=5, shuffle=True)
for fold_idx, (train_idx, test_idx) in enumerate(kf.split(X_source), 1):
    print(f"Fold {fold_idx}")
    # split data
    x_train, x_val = X_source.iloc[train_idx, :], X_source.iloc[test_idx, :]
    y_train, y_val = Y_source.iloc[train_idx, :], Y_source.iloc[test_idx, :]

    # get per-class predictions
    # add the predictions on source data
    task_models = []
    for task in tasks:
        best_model = xgb.XGBClassifier(n_jobs=-1).fit(x_train, y_train[task])
        task_models.append(best_model)
        scores = score_per_class(best_model, x_val.values, y_val[task].values)
        for score_idx, score in enumerate(scores):
            results.append(
                {
                    "fold": fold_idx,
                    "task": task,
                    "task_label": score_idx,
                    "accuracy": score,
                    # "data": "normalised",
                    "type": "source",
                }
            )
    # add for all target datasets
    for target_idx, target in enumerate(data_targets):
        for task_idx, task in enumerate(tasks):
            scores = score_per_class(
                task_models[task_idx], target[0].values, target[1][task].values
            )
            for score_idx, score in enumerate(scores):
                results.append(
                    {
                        "fold": fold_idx,
                        "task": task,
                        "task_label": score_idx,
                        "accuracy": score,
                        # "data": "h",
                        "type": "target",
                        "target": target_idx,
                    }
                )

    # add machine predictin on source as extra column
    machine_pred = (
        xgb.XGBClassifier()
        .fit(x_train.values, y_train["Analyser ID"].values)
        .score(x_val.values, y_val["Analyser ID"].values)
    )
    results.append(
        {
            "fold": fold_idx,
            "task": "Source Machine Prediction",
            # "task_label": 0,
            "accuracy": machine_pred,
            # "data": "normalised",
            "type": "source",
        }
    )

raw_crossval_df = pd.DataFrame(results)
raw_crossval_df.to_csv("highdim_crossval.csv")


###############################################################
# SAME AGAIN FOR AUTOENCODER
###############################################################
print("Starting autoencoder...")
results = []
ae_models = []
trainers = []
kf = KFold(n_splits=5, shuffle=True)
for fold_idx, (train_idx, test_idx) in enumerate(kf.split(X_source), 1):
    print(f"Fold {fold_idx}")
    # split data
    x_train, x_val = X_source.iloc[train_idx, :], X_source.iloc[test_idx, :]
    y_train, y_val = Y_source.iloc[train_idx, :], Y_source.iloc[test_idx, :]
    # make dataloaders
    train_loader = models.dataloader(x_train, y_train, batch_size=batch_size)
    val_loader = models.dataloader(x_val, y_val, batch_size=batch_size)

    # train 5 times and keep the best model
    best_model = None
    for i in range(5):
        early_stop_callback = EarlyStopping(
            monitor="val_loss",
            min_delta=0.00,
            patience=batch_size / 8,
            verbose=False,
            mode="min",
        )
        trainer = pl.Trainer(
            max_epochs=10_000,
            # accelerator="cpu",
            # devices=32,
            callbacks=[early_stop_callback],
            enable_progress_bar=False,
            enable_checkpointing=False,
        )
        model = models.LitAE(
            kernels=[64, 64, 32],
            input_dim=x_train.shape[1],
            overall_act="ReLU",
            latent_act="ReLU",
            output_act="None",
            scheduler_patience=20,
            lr=0.1,
        )
        print(f"Training AE model for fold {fold_idx} and run {i+1}...")
        trainer.fit(
            model=model, train_dataloaders=train_loader, val_dataloaders=val_loader
        )
        # model.best_loss = trainer.callback_metrics["val_loss"].min()
        model.eval()
        model.best_loss = F.mse_loss(
            model._run_step(torch.Tensor(X_source.values)),
            torch.Tensor(X_source.values),
        ).item()

        print("Current model loss:", model.best_loss)

        if best_model is None:
            best_model = model
            best_trainer = trainer
        elif model.best_loss < best_model.best_loss:
            best_model = model
            best_trainer = trainer

    ae_models.append(best_model)
    trainers.append(best_trainer)

    best_model.eval()
    with torch.no_grad():
        source_embed = best_model(torch.Tensor(X_source.values))
        train_embed = best_model(torch.Tensor(x_train.values))
        val_embed = best_model(torch.Tensor(x_val.values))
        target_embeds = []
        for target in data_targets:
            target_embeds.append(best_model(torch.Tensor(target[0].values)))

    # selection score on source data
    with torch.no_grad():
        reconstructions = best_model._run_step(torch.Tensor(X_source.values)).detach()

    # # for debugging, save some plots of original vs reconstruction
    # if fold_idx == 1:
    #     for i in range(10):
    #         x = np.arange(0, X_source.shape[1])
    #         plt.plot(x, X_source.values[i], label="original")
    #         plt.plot(x, reconstructions[i], label="reconstruction")
    #         plt.legend()
    #         plt.savefig(f"reconstructions_{i}.png")
    #         plt.close()

    print("Calculating AE model selection score...")
    ae_score, ae_accuracy, ae_variation, ae_reconstruction = model_selection_score(
        originals=X_source.values,
        reconstructions=reconstructions,
        embeddings=source_embed.detach().numpy(),
        tasks_labels=Y_source[
            [
                "Sex",
                "age_bracket",
                "bmi_bracket",
            ]
        ].values,
        domains_labels=Y_source[
            [
                "Analyser ID",
                "vendelay",
                "season",
            ]
        ].values,
        distance_metric=ot.emd2_1d,
        n_betas=1000,
    )
    results.append(
        {
            "fold": fold_idx,
            "selection_score": ae_score,
            "selection_accuracy": ae_accuracy,
            "selection_variation": ae_variation,
            "selection_reconstruction": ae_reconstruction,
            "type": "source",
        }
    )

    print("Calculating AE per-class accuracies...")
    # get per-class predictions
    # add the predictions on source data
    task_models = []
    for task in tasks:
        classer = xgb.XGBClassifier(n_jobs=-1).fit(
            train_embed.detach().numpy(), y_train[task].values
        )
        task_models.append(classer)
        scores = score_per_class(
            classer, val_embed.detach().numpy(), y_val[task].values
        )
        for score_idx, score in enumerate(scores):
            results.append(
                {
                    "fold": fold_idx,
                    "task": task,
                    "task_label": score_idx,
                    "accuracy": score,
                    # "data": "normalised",
                    "type": "source",
                }
            )
    # add for all target datasets
    for target_idx, target in enumerate(target_embeds):
        for task_idx, task in enumerate(tasks):
            scores = score_per_class(
                task_models[task_idx],
                target.detach().numpy(),
                data_targets[target_idx][1][task].values,
            )
            for score_idx, score in enumerate(scores):
                results.append(
                    {
                        "fold": fold_idx,
                        "task": task,
                        "task_label": score_idx,
                        "accuracy": score,
                        # "data": "h",
                        "type": "target",
                        "target": target_idx,
                    }
                )

    print("Calculating AE machine prediction accuracy on source data...")
    # add machine predictin on source as extra column
    machine_pred = (
        xgb.XGBClassifier()
        .fit(train_embed.detach().numpy(), y_train["Analyser ID"].values)
        .score(val_embed.detach().numpy(), y_val["Analyser ID"].values)
    )
    results.append(
        {
            "fold": fold_idx,
            "task": "Source Machine Prediction",
            # "task_label": 0,
            "accuracy": machine_pred,
            # "data": "normalised",
            "type": "source",
        }
    )
    print("Calculating AE variation for source and source+target...")
    # add variations for AE latent space for all domains calculating for source and source+target
    for i, domain in enumerate(domains):
        with torch.no_grad():
            variation = variation_different_targets(
                dataset=data,
                domains_labels=[domain],
                tasks_labels=tasks,
                target_label="Analyser ID",
                n_betas=1000,
                source_labels=list(Y_source["Analyser ID"].unique()),
                distance_metric=fast_jensen_shannon_divergence_1d,
                model=best_model,
            )
        results.append(
            {
                "fold": fold_idx,
                "domain": domain,
                "variation": variation[0],
                "type": "source",
            }
        )
        for target_idx, _ in enumerate(data_targets):
            results.append(
                {
                    "fold": fold_idx,
                    "domain": domain,
                    "variation": variation[target_idx + 1],
                    "type": "target",
                    "target": target_idx,
                }
            )

ae_crossval_df = pd.DataFrame(results)
ae_crossval_df.to_csv("ae_crossval.csv", index=False)

#################################
# AE DATA PHATE PLOTS
#################################
# sample_X = data.X.sample(n=100_000, random_state=42)
# sample_Y = data.Y.loc[sample_X.index]

# get best AE model (lowest loss)
best_model = None
for i, model in enumerate(ae_models):
    if best_model is None:
        best_model = model
        best_trainer = trainers[i]
    elif model.best_loss < best_model.best_loss:
        best_model = model
        best_trainer = trainers[i]

best_trainer.save_checkpoint("best_ae_model.ckpt")

best_model.eval()
with torch.no_grad():
    all_embed = best_model(torch.Tensor(data.X.values))

print("Making PHATE plots using data of shape: ", all_embed.shape)

phate_operator_raw = phate.PHATE(n_jobs=-1)
ae_phate = phate_operator_raw.fit_transform(all_embed.detach().numpy())
g = make_PHATE_plot(
    hue_var="Sex",
    y_data=data.Y,
    encoder_list=Y_encoder.encoders,
    phate_array=ae_phate,
)

# plt.xlabel("PHATE1")
# plt.ylabel("PHATE2")
plt.xticks([])
plt.yticks([])
# plt.title("Scatterplot of PHATE-embedded latent space")

# these are matplotlib.patch.Patch properties
props = dict(boxstyle="round", facecolor="white", alpha=1.0, edgecolor="black")

# place a text box in upper left in axes coords
g.axes.text(
    0.02,
    0.07,
    "Vanilla AE Latent Space",
    transform=g.axes.transAxes,
    fontsize=14,
    verticalalignment="top",
    bbox=props,
)

plt.legend(title="Sex", loc="lower right")

plt.tight_layout()

plt.savefig("FBC_AE_PHATE_sex.png")
plt.close()

g = make_PHATE_plot(
    hue_var="Analyser ID",
    y_data=data.Y,
    encoder_list=Y_encoder.encoders,
    phate_array=ae_phate,
    hue_order=["INTERVAL 1", "INTERVAL 2", "COMPARE"],
)

# plt.xlabel("PHATE1")
# plt.ylabel("PHATE2")
plt.xticks([])
plt.yticks([])
# plt.title("Scatterplot of PHATE-embedded latent space")

# these are matplotlib.patch.Patch properties
props = dict(boxstyle="round", facecolor="white", alpha=1.0, edgecolor="black")

# place a text box in upper left in axes coords
g.axes.text(
    0.02,
    0.07,
    "Vanilla AE Latent Space",
    transform=g.axes.transAxes,
    fontsize=14,
    verticalalignment="top",
    bbox=props,
)

plt.legend(
    title="Analyser",
    loc="lower right",
    #    labels=["INTERVAL 1", "INTERVAL 2", "COMPARE"]
)

plt.tight_layout()

plt.savefig("FBC_AE_PHATE_machine.png")
plt.close()

#################################

###############################################################
# SAME AGAIN FOR Dis-AE
###############################################################
print("Starting Dis-AE...")
results = []
disae_models = []
kf = KFold(n_splits=5, shuffle=True)
for fold_idx, (train_idx, test_idx) in enumerate(kf.split(X_source), 1):
    print(f"Fold {fold_idx}")
    # split data
    x_train, x_val = X_source.iloc[train_idx, :], X_source.iloc[test_idx, :]
    y_train, y_val = Y_source.iloc[train_idx, :], Y_source.iloc[test_idx, :]
    # concat for Dis-AE format
    train_data = pd.concat([x_train, y_train], axis=1)
    val_data = pd.concat([x_val, y_val], axis=1)

    # train 5 times and keep the best model
    best_model = None
    for i in range(5):
        selfdis_ae_model = models.Sam_DisAE(
            heads=[
                ["Sex", True, True],
                ["Analyser ID", False, True],
                #         ["Ethnicity", False, True],
                #         ["d_week", False, True],
                #         ["t_day", False, True],
                ["vendelay", False, True],
                ["season", False, True],
                ["age_bracket", True, True],
                ["bmi_bracket", True, True],
            ],
            layers=[83, 64, 64, 32],
            reconstruction_factor=9.055,
            task_factor=0.3279,
            domain_factor=2.939,
            #     domain_factor=10.,
        )
        print(f"Training Dis-AE model for fold {fold_idx} and run {i+1}...")
        selfdis_ae_model.fit(
            data=train_data,
            X_columns=feats_rich,
            batch_size=batch_size,
            weight_decay=0.0001,
            lr_0=0.001,
            #     patience=batch_size / 16,
            save_model=False,
        )
        # selection score on source data
        with torch.no_grad():
            source_embed = selfdis_ae_model(torch.Tensor(X_source.values))
            reconstructions = selfdis_ae_model.decoder(source_embed).detach()

        # print("Calculating Dis-AE model selection score...")
        selfdis_ae_model.score, _, _, _ = model_selection_score(
            originals=X_source.values,
            reconstructions=reconstructions,
            embeddings=source_embed.detach().numpy(),
            tasks_labels=Y_source[
                [
                    "Sex",
                    "age_bracket",
                    "bmi_bracket",
                ]
            ].values,
            domains_labels=Y_source[
                [
                    "Analyser ID",
                    "vendelay",
                    "season",
                ]
            ].values,
            distance_metric=ot.emd2_1d,
            n_betas=1000,
        )
        if best_model is None:
            best_model = selfdis_ae_model
        elif selfdis_ae_model.score > best_model.score:
            best_model = selfdis_ae_model

    disae_models.append(best_model)

    with torch.no_grad():
        source_embed = best_model(torch.Tensor(X_source.values))
        train_embed = best_model(torch.Tensor(x_train.values))
        val_embed = best_model(torch.Tensor(x_val.values))
        target_embeds = []
        for target in data_targets:
            target_embeds.append(best_model(torch.Tensor(target[0].values)))

    # selection score on source data
    with torch.no_grad():
        reconstructions = best_model.decoder(source_embed).detach()

    print("Calculating Dis-AE model selection score...")
    ae_score, ae_accuracy, ae_variation, ae_reconstruction = model_selection_score(
        originals=X_source.values,
        reconstructions=reconstructions,
        embeddings=source_embed.detach().numpy(),
        tasks_labels=Y_source[
            [
                "Sex",
                "age_bracket",
                "bmi_bracket",
            ]
        ].values,
        domains_labels=Y_source[
            [
                "Analyser ID",
                "vendelay",
                "season",
            ]
        ].values,
        distance_metric=ot.emd2_1d,
        n_betas=1000,
    )
    results.append(
        {
            "fold": fold_idx,
            "selection_score": ae_score,
            "selection_accuracy": ae_accuracy,
            "selection_variation": ae_variation,
            "selection_reconstruction": ae_reconstruction,
            "type": "source",
        }
    )

    print("Calculating Dis-AE per-class accuracies...")
    # get per-class predictions
    # add the predictions on source data
    task_models = []
    for task in tasks:
        classer = xgb.XGBClassifier(n_jobs=-1).fit(
            train_embed.detach().numpy(), y_train[task].values
        )
        task_models.append(classer)
        scores = score_per_class(
            classer, val_embed.detach().numpy(), y_val[task].values
        )
        for score_idx, score in enumerate(scores):
            results.append(
                {
                    "fold": fold_idx,
                    "task": task,
                    "task_label": score_idx,
                    "accuracy": score,
                    # "data": "normalised",
                    "type": "source",
                }
            )
    # add for all target datasets
    for target_idx, target in enumerate(target_embeds):
        for task_idx, task in enumerate(tasks):
            scores = score_per_class(
                task_models[task_idx],
                target.detach().numpy(),
                data_targets[target_idx][1][task].values,
            )
            for score_idx, score in enumerate(scores):
                results.append(
                    {
                        "fold": fold_idx,
                        "task": task,
                        "task_label": score_idx,
                        "accuracy": score,
                        # "data": "h",
                        "type": "target",
                        "target": target_idx,
                    }
                )

    print("Calculating Dis-AE machine prediction accuracy on source data...")
    # add machine predictin on source as extra column
    machine_pred = (
        xgb.XGBClassifier()
        .fit(train_embed.detach().numpy(), y_train["Analyser ID"].values)
        .score(val_embed.detach().numpy(), y_val["Analyser ID"].values)
    )
    results.append(
        {
            "fold": fold_idx,
            "task": "Source Machine Prediction",
            # "task_label": 0,
            "accuracy": machine_pred,
            # "data": "normalised",
            "type": "source",
        }
    )
    print("Calculating Dis-AE variation for source and source+target...")
    # add variations for AE latent space for all domains calculating for source and source+target
    for i, domain in enumerate(domains):
        with torch.no_grad():
            variation = variation_different_targets(
                dataset=data,
                domains_labels=[domain],
                tasks_labels=tasks,
                target_label="Analyser ID",
                n_betas=1000,
                source_labels=list(Y_source["Analyser ID"].unique()),
                distance_metric=fast_jensen_shannon_divergence_1d,
                model=best_model,
            )
        results.append(
            {
                "fold": fold_idx,
                "domain": domain,
                "variation": variation[0],
                "type": "source",
            }
        )
        for target_idx, _ in enumerate(data_targets):
            results.append(
                {
                    "fold": fold_idx,
                    "domain": domain,
                    "variation": variation[target_idx + 1],
                    "type": "target",
                    "target": target_idx,
                }
            )

disae_crossval_df = pd.DataFrame(results)
disae_crossval_df.to_csv("disae_crossval.csv", index=False)

#################################
# Dis-AE DATA PHATE PLOTS
#################################
# sample_X = data.X.sample(n=100_000, random_state=42)
# sample_Y = data.Y.loc[sample_X.index]

# get best AE model (lowest loss)
best_model = None
for model in disae_models:
    if best_model is None:
        best_model = model
    elif model.score > best_model.score:
        best_model = model

best_model.save_model("best_disae_model")
# best_model.eval()
with torch.no_grad():
    all_embed = best_model(torch.Tensor(data.X.values))

print("Making PHATE plots using data of shape: ", all_embed.shape)

phate_operator_raw = phate.PHATE(n_jobs=-1)
disae_phate = phate_operator_raw.fit_transform(all_embed.detach().numpy())
g = make_PHATE_plot(
    hue_var="Sex",
    y_data=data.Y,
    encoder_list=Y_encoder.encoders,
    phate_array=disae_phate,
)

# plt.xlabel("PHATE1")
# plt.ylabel("PHATE2")
plt.xticks([])
plt.yticks([])
# plt.title("Scatterplot of PHATE-embedded latent space")

# these are matplotlib.patch.Patch properties
props = dict(boxstyle="round", facecolor="white", alpha=1.0, edgecolor="black")

# place a text box in upper left in axes coords
g.axes.text(
    0.02,
    0.07,
    "Dis-AE Latent Space",
    transform=g.axes.transAxes,
    fontsize=14,
    verticalalignment="top",
    bbox=props,
)

plt.legend(title="Sex", loc="lower right")

plt.tight_layout()

plt.savefig("FBC_Dis-AE_PHATE_sex.png")
plt.close()

g = make_PHATE_plot(
    hue_var="Analyser ID",
    y_data=data.Y,
    encoder_list=Y_encoder.encoders,
    phate_array=disae_phate,
    hue_order=["INTERVAL 1", "INTERVAL 2", "COMPARE"],
)

# plt.xlabel("PHATE1")
# plt.ylabel("PHATE2")
plt.xticks([])
plt.yticks([])
# plt.title("Scatterplot of PHATE-embedded latent space")

# these are matplotlib.patch.Patch properties
props = dict(boxstyle="round", facecolor="white", alpha=1.0, edgecolor="black")

# place a text box in upper left in axes coords
g.axes.text(
    0.02,
    0.07,
    "Dis-AE Latent Space",
    transform=g.axes.transAxes,
    fontsize=14,
    verticalalignment="top",
    bbox=props,
)

plt.legend(
    title="Analyser",
    loc="lower right",
    #    labels=["INTERVAL 1", "INTERVAL 2", "COMPARE"]
)

plt.tight_layout()

plt.savefig("FBC_Dis-AE_PHATE_machine.png")
plt.close()

#################################
print("... done. Script successful.")
