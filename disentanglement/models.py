############
# INIT
############
import os
import pickle
from collections import OrderedDict
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pytorch_lightning as pl
import torch
import torch.nn.functional as F
import yaml
from sklearn.metrics import accuracy_score, r2_score
from sklearn.model_selection import KFold, cross_val_score, train_test_split
from torch import nn, optim
from tqdm import tqdm
from xgboost import XGBClassifier, XGBRegressor

from .training_utils import (
    GradientReversal,
)

####################
# UTILITY FUNCTIONS
####################


def determine_act(act_str):
    # function to return activation function from specifying string
    if act_str == "ReLU":
        return nn.ReLU()
    if act_str == "Sigmoid":
        return nn.Sigmoid()
    if act_str == "Tanh":
        return nn.Tanh()


def dataloader(X, Y, batch_size=None, balance_on=None):
    dataset = CustomDataset(X, Y)
    if batch_size == None:
        batch_size = len(dataset)
    if balance_on != None:
        _, counts = np.unique(Y[balance_on], return_counts=True)
        weights = [sum(counts) / c for c in counts]
        example_weights = [weights[i] for i in Y[balance_on].astype(int)]
        sampler = torch.utils.data.WeightedRandomSampler(example_weights, len(dataset))
        dataloader = torch.utils.data.DataLoader(
            dataset,
            sampler=sampler,
            batch_size=batch_size,
        )
    else:
        dataloader = torch.utils.data.DataLoader(
            dataset,
            shuffle=True,
            batch_size=batch_size,
        )
    return dataloader


def get_lr(optimizer):
    for param_group in optimizer.param_groups:
        return param_group["lr"]


def xgb_accuracy(X, y):
    y = pd.factorize(y)[0]
    kfold = KFold(n_splits=5)
    return cross_val_score(
        XGBClassifier(use_label_encoder=False, verbosity=0),
        X,
        y,
        cv=kfold,
        scoring="accuracy",
    ).mean()


def xgb_regression(X, y):
    kfold = KFold(n_splits=5)
    return cross_val_score(
        XGBRegressor(verbosity=0),
        X,
        y,
        cv=kfold,
    ).mean()


###############
# Models
###############


class LitAE(pl.LightningModule):
    """
    Creates PyTorch-Lightning Autoencoder Neural Network using fully-connected layers only.
    Activations are variable.

    PARAMS:
    kernels: List of kernels for each layer (specifies kernel size as well as number of hidden layers).
             The last entry in kernels will be the latent feature space.
             Only the encoder layers have to be specified; the decoder will automatically
             mirror the architecture of the encoder.
    input_dim: number of input neurons
    lr: learning rate of Adam optimiser
    """

    def __init__(
        self,
        kernels,
        input_dim: int,
        lr: float = 1e-3,
        overall_act: str = "ReLU",
        latent_act: str = "None",
        output_act: str = "Sigmoid",
        scheduler_patience: int = 10,
    ):
        super().__init__()

        self.save_hyperparameters()

        self.lr = lr
        self.input_dim = input_dim
        self.overall_act = overall_act
        self.latent_act = latent_act
        self.output_act = output_act
        self.scheduler_patience = scheduler_patience

        act = determine_act(overall_act)

        enc_dict = OrderedDict(
            [
                ("enc_layer_0", nn.Linear(self.input_dim, kernels[0])),
                ("act_0", act),
                ("norm_layer_0", nn.BatchNorm1d(kernels[0])),
            ]
        )

        for k in range(len(kernels) - 2):
            enc_dict["enc_layer_" + str(k + 1)] = nn.Linear(kernels[k], kernels[k + 1])
            enc_dict["act_" + str(k + 1)] = act
            enc_dict["norm_layer_" + str(k + 1)] = nn.BatchNorm1d(kernels[k + 1])

        self.encoder = nn.Sequential(enc_dict)

        if latent_act == "None":
            self.fc_latent = nn.Sequential(
                nn.Linear(kernels[-2], kernels[-1]), nn.BatchNorm1d(kernels[-1])
            )
        else:
            self.fc_latent = nn.Sequential(
                nn.Linear(kernels[-2], kernels[-1]),
                determine_act(latent_act),
                nn.BatchNorm1d(kernels[-1]),
            )

        dec_dict = OrderedDict([])

        for k in range(1, len(kernels)):
            dec_dict["dec_layer_" + str(k)] = nn.Linear(kernels[-k], kernels[-k - 1])
            dec_dict["act_" + str(k)] = act
            dec_dict["norm_layer_" + str(k)] = nn.BatchNorm1d(kernels[-k - 1])

        dec_dict["dec_layer_" + str(len(kernels))] = nn.Linear(
            kernels[0], self.input_dim
        )
        if output_act != "None":
            dec_dict["act_" + str(len(kernels))] = determine_act(output_act)

        self.decoder = nn.Sequential(dec_dict)

    def forward(self, x):
        x = self.encoder(x)
        z = self.fc_latent(x)
        return z

    def _run_step(self, x):
        x = self.encoder(x)
        z = self.fc_latent(x)
        x_hat = z
        x_hat = self.decoder(x_hat)

        return x_hat

    def step(self, batch, batch_idx):
        x, _ = batch
        x_hat = self._run_step(x)

        recon_loss = torch.nn.MSELoss()(x_hat, x)

        loss = recon_loss

        logs = {
            "loss": loss,
        }
        return loss, logs

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.lr, weight_decay=1e-5)
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
            optimizer, "min", patience=self.scheduler_patience, min_lr=1e-6
        )
        return {
            "optimizer": optimizer,
            "lr_scheduler": {
                "scheduler": scheduler,
                "monitor": "val_loss",
                "frequency": 1,
            },
        }

    def training_step(self, batch, batch_idx):
        loss, logs = self.step(batch, batch_idx)
        self.log_dict(
            {f"train_{k}": v for k, v in logs.items()}, on_step=True, on_epoch=False
        )
        return loss

    def validation_step(self, batch, batch_idx):
        loss, logs = self.step(batch, batch_idx)
        self.log_dict({f"val_{k}": v for k, v in logs.items()})
        return loss


# Sam's new model
class torchModel(nn.Module):
    # for encoder and decoder
    def __init__(self, layers, activation=nn.ReLU()):
        super().__init__()
        self.layers = []
        for i in range(len(layers) - 1):
            self.layers.append(nn.Linear(layers[i], layers[i + 1]))
            self.layers.append(activation)
            self.layers.append(nn.BatchNorm1d(layers[i + 1]))
        self.layers = nn.Sequential(*self.layers[:-2])

    def forward(self, x):
        return self.layers(x)


class CustomDataset(torch.utils.data.Dataset):
    def __init__(self, X: pd.DataFrame, Y: pd.DataFrame):
        self.X = torch.tensor(X.values).float()
        self.Y = torch.tensor(Y.values).long()

    def __getitem__(self, idx):
        return self.X[idx], self.Y[idx]

    def __len__(self):
        return len(self.X)


class Sam_DisAE:
    def __init__(
        self,
        heads=[],
        layers=None,
        device: str = "cpu",
        reconstruction_factor: float = 1.0,
        task_factor: float = 1.0,
        domain_factor: float = 1.0,
    ):
        self.reconstruction_factor = reconstruction_factor
        self.task_factor = task_factor
        self.domain_factor = domain_factor
        self.set_heads(heads, self.domain_factor)
        self.layers = layers
        if layers:
            self.set_layers(layers)
        self.device = device
        self._is_trained = False

    def set_heads(self, heads, domain_factor):
        self.heads = {}
        for head in heads:
            self.add_head(*head, domain_factor)

    def add_head(self, key, isTask, isCategorical, domain_factor):
        assert key not in self.heads, f"head: {key} already initiated"
        self.heads[key] = Head(isTask, isCategorical, grad_lambda=domain_factor)

    def set_layers(self, layers):
        for layer in layers:
            assert isinstance(layer, int)
        self.layers = layers

    def fit(
        self,
        data: pd.DataFrame,
        X_columns: list,
        layers=None,
        device: str = None,
        lr_0: float = 0.1,
        epochs: int = 1000,
        batch_size: int = 1024,
        start_scheduling: int = 0,
        weight_decay: float = 0.0001,
        patience: int = 25,
        save_model: bool = True,
        balance_on: str = None,
    ):
        if layers:
            self.set_layers(layers)
        if device:
            self.device = device

        self.X_columns = X_columns
        self.weight_decay = weight_decay
        self.batch_size = batch_size
        self.lr_0 = lr_0

        self.encoder = torchModel(self.layers).to(self.device)
        self.decoder = torchModel(self.layers[::-1]).to(self.device)
        params = list(self.encoder.parameters()) + list(self.decoder.parameters())
        for row, head in self.heads.items():
            if head.isCategorical:
                head.set_map(set(data[row]))
                data[row] = data[row].map(head.map)
            head.create_model(self.layers[-1])
            head.to(self.device)
            params += list(head.parameters())

        self.optimiser = optim.Adam(
            params, lr=self.lr_0, weight_decay=self.weight_decay
        )
        self.scheduler = optim.lr_scheduler.ReduceLROnPlateau(
            self.optimiser, "min", patience=patience
        )

        X = data[self.X_columns]
        Y = data[list(self.heads)]
        X_train, X_val, Y_train, Y_val = train_test_split(X, Y, test_size=0.2)
        self.train_dataloader = dataloader(
            X_train, Y_train, self.batch_size, balance_on=balance_on
        )
        self.val_dataloader = dataloader(X_val, Y_val)

        self.train_losses = []
        self.val_losses = []
        self.learning_rates = []
        for epoch in tqdm(range(1, epochs + 1)):
            self.learning_rates.append(get_lr(self.optimiser))
            if get_lr(self.optimiser) < 1e-7:
                break
            self.train()
            if epoch > start_scheduling:
                self.val()
            """if epoch % 25 == 0:
                print(epoch, get_lr(self.optimiser))
                print("sanity check, val")
                self.sanity_check(X_val, Y_val)"""
        self._is_trained = True
        if save_model:
            self.save_model()

    def train(self):
        self.encoder.train()
        self.decoder.train()
        for head in self.heads.values():
            head.train()

        for X, Y in self.train_dataloader:
            X = X.to(self.device)
            Y = Y.to(self.device)
            Z = self.encoder(X.to(self.device))
            X_reconstructed = self.decoder(Z)
            loss = nn.MSELoss()(X_reconstructed, X)
            (self.reconstruction_factor * loss).backward(retain_graph=True)
            self.train_losses.append(loss.item())

            for i, head in enumerate(self.heads.values()):
                y_true = Y[:, i]
                y_pred = head(Z)
                if head.isCategorical:
                    loss = nn.CrossEntropyLoss()(y_pred, y_true.long())
                else:
                    loss = nn.MSELoss()(y_pred.squeeze(), y_true)

                if head.isTask:
                    loss = self.task_factor * loss
                loss.backward(retain_graph=True)
                head.train_losses.append(loss.item())

            self.optimiser.step()
            self.optimiser.zero_grad()

    def val(self):
        self.encoder.eval()
        self.decoder.eval()
        X, Y = next(iter(self.val_dataloader))
        X = X.to(self.device)
        Y = Y.to(self.device)
        Z = self.encoder(X)
        X_reconstructed = self.decoder(Z)
        sum_loss = 0
        loss = nn.MSELoss()(X_reconstructed, X)
        self.val_losses.append(loss.item())
        sum_loss += self.reconstruction_factor * loss

        for i, head in enumerate(self.heads.values()):
            head.eval()
            y_true = Y[:, i]
            y_pred = head(Z)
            if head.isCategorical:
                loss = nn.CrossEntropyLoss()(y_pred, y_true.long())
            else:
                loss = nn.MSELoss()(y_pred.squeeze(), y_true)
            head.val_losses.append(loss.item())
            if head.isTask:
                sum_loss += loss

        self.scheduler.step(sum_loss)

    def sanity_check(self, X: pd.DataFrame, Y: pd.DataFrame):
        self.encoder.eval()
        with torch.no_grad():
            Z = self.encoder(torch.tensor(X.values).float().to(self.device))
        for key, head in self.heads.items():
            print(key, end=": ")
            print(head)
            head.eval()

            if head.isCategorical:
                xgb = xgb_accuracy(Z.cpu().numpy(), Y[key])
                model = accuracy_score(head.predict(Z), Y[key])
            else:
                xgb = xgb_regression(Z.cpu().numpy(), Y[key])
                model = r2_score(head.predict(Z), Y[key])
            print(f"xgb: {xgb:.3f}, model: {model:.3f}")

    def save_model(self, path: str = None):
        if not self._is_trained:
            raise Exception("Model not trained yet")
        if path is None:
            dt_string = datetime.now().strftime("%d%m%Y_%H%M%S")
            path = f"./disae_checkpoints/{dt_string}"
        os.makedirs(path, exist_ok=True)
        with open(path + "/full_model.pkl", "wb") as f:
            pickle.dump(self, f)
        torch.save(self.encoder.state_dict(), path + "/encoder.pt")
        torch.save(self.decoder.state_dict(), path + "/decoder.pt")
        for key, head in self.heads.items():
            torch.save(head.state_dict(), path + f"/{key}_head.pt")
        self.hyperparameters = {
            "lr_0": self.lr_0,
            "batch_size": self.batch_size,
            "weight_decay": self.weight_decay,
            "reconstruction_factor": self.reconstruction_factor,
            "task_factor": self.task_factor,
            "domain_factor": self.domain_factor,
            "layers": self.layers,
        }
        with open(path + "/hyperparameters.yaml", "w") as file:
            yaml.dump(self.hyperparameters, file)

        print("Model saved to", path)

    def plot_losses(self):
        plt.plot(self.train_losses, label="train")
        plt.plot(self.val_losses, label="val")
        plt.legend()
        plt.show()

    def __str__(self) -> str:
        return "DisAE Model" + "".join(
            f"\n {key} {head.__str__()}" for key, head in self.heads.items()
        )

    def __call__(self, X):
        try:
            self.encoder.eval()
        except:
            raise AttributeError("Error, in DisAE.__call__, model is not trained yet")
        if isinstance(X, pd.DataFrame):
            X = X[self.X_columns]
            X = torch.tensor(X.values).float()
        else:
            assert isinstance(X, torch.Tensor)

        with torch.no_grad():
            return self.encoder(X.to(self.device)).cpu()


class Head(nn.Module):
    def __init__(
        self, isTask: bool, isCategorical: bool, grad_lambda: float = 1.0
    ) -> None:
        super().__init__()
        self.isTask = isTask
        self.isCategorical = isCategorical
        self.map = {}
        self.train_losses = []
        self.val_losses = []
        self.criterion = nn.CrossEntropyLoss() if self.isCategorical else nn.MSELoss()
        self.grad_lambda = grad_lambda

    def set_map(self, seen):
        for i, x in enumerate(set(seen)):
            self.map[x] = i

    def create_model(self, in_dim, activation=nn.ReLU()):
        out_dim = len(self.map) if self.isCategorical else 1
        self.layers = nn.Sequential(
            GradientReversal(alpha=self.grad_lambda)
            if not self.isTask
            else nn.Identity(),
            nn.Linear(in_dim, in_dim),
            activation,
            nn.BatchNorm1d(in_dim),
            nn.Linear(in_dim, out_dim),
        )

    def forward(self, x):
        return self.layers(x)

    def predict_proba(self, Z):
        assert self.isCategorical, "head is not categorical"
        self.eval()
        with torch.no_grad():
            return F.softmax(self(Z), 1).cpu().numpy()

    def predict(self, Z):
        self.eval()
        with torch.no_grad():
            if self.isCategorical:
                return self(Z).argmax(1).cpu().numpy()
            return self(Z).cpu().numpy()

    def __str__(self) -> str:
        type = "Categorical" if self.isCategorical else "Continuous"
        goal = "Task" if self.isTask else "Domain"
        return f"Head: {type}, {goal}"
