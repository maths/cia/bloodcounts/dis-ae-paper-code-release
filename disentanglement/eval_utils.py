#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@time    :   2022/08/15 11:19:34
@author  :   Daniel Kreuter & Samuel Tull
@version :   1.0
@contact :   dk659@cam.ac.uk, st862@cam.ac.uk
@desc    :   Functions to evaluate/plot model outputs
"""

######################
# INIT
######################

import itertools
import warnings

import numpy as np
import numpy.typing as npt
import ot
import pandas as pd
import seaborn as sns
import torch
import xgboost as xgb
from joblib import Parallel, delayed
from scipy.integrate import quad
from scipy.spatial.distance import jensenshannon
from scipy.stats import gaussian_kde
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import cross_val_score
from tqdm import tqdm

from ._math_functions import gaussian_kernel
from .training_utils import Dataset

######################
# FUNCTIONS
######################


def make_PHATE_plot(
    hue_var: str,
    y_data,
    encoder_list,
    phate_array,
    hue_order=None,
    subsample_size: int = 1000,
):
    col_index = y_data.columns.get_loc(hue_var)
    hue_labels = y_data[hue_var].astype(int)
    _, counts = np.unique(hue_labels, return_counts=True)
    if np.all(counts >= subsample_size):
        warnings.warn("Too many points to plot, subsampling...")
        indices = []
        for category in np.unique(hue_labels):
            indices.append(np.where(hue_labels == category)[0])
        sampled_indices_cat = []
        for category in indices:
            sampled_indices_cat.append(
                np.random.choice(category, size=subsample_size, replace=False)
            )
        sampled_indices = np.concatenate(sampled_indices_cat)
        hue_labels = hue_labels.iloc[sampled_indices]
        phate_array = phate_array[sampled_indices]

    try:
        hue_labels.replace(
            to_replace=dict(enumerate(encoder_list[col_index].classes_)), inplace=True
        )
    except AttributeError:
        pass

    return sns.scatterplot(
        x=phate_array[:, 0],
        y=phate_array[:, 1],
        hue=hue_labels,
        s=10.0,
        hue_order=hue_order,
        # style=hue_labels,
        # alpha=0.2,
        #     legend=False,
    )


def score_per_class(model, x_test: np.ndarray, y_test: np.ndarray) -> np.ndarray:
    y_pred = model.predict(x_test)
    y_true = y_test
    matrix = confusion_matrix(y_true, y_pred)
    return matrix.diagonal() / matrix.sum(axis=1)


def plot_median_and_quartiles(df: pd.core.frame.DataFrame, x: str, y: str, **kwargs):
    # plots median and central quartile of y over x given dataframe df
    data_stats = df.groupby(x).describe()

    x = data_stats.index
    medians = data_stats[(y, "50%")]
    medians.name = y
    quartiles1 = data_stats[(y, "25%")]
    quartiles3 = data_stats[(y, "75%")]

    ax = sns.lineplot(x=x, y=medians, **kwargs)
    ax.fill_between(x, quartiles1, quartiles3, alpha=0.3)


def custom_round(x, base=5):
    return base * round(float(x) / base)


def model_selection_score(
    originals: np.ndarray,
    reconstructions,
    embeddings: np.ndarray,
    tasks_labels: np.ndarray,
    domains_labels: np.ndarray,
    distance_metric=None,
    n_betas: int = 1,
):
    if distance_metric is None:
        distance_metric = jensen_shannon_divergence_1d

    print("Getting accuracies...")
    accuracies = []
    for task_col in tasks_labels.T:
        accuracies.append(
            np.mean(
                cross_val_score(
                    estimator=xgb.XGBClassifier(verbosity=0),
                    X=embeddings,
                    y=task_col,
                )
            )
        )

    print("Getting variations...")
    variation = model_variation(
        embeddings=embeddings,
        tasks_labels=tasks_labels,
        domains_labels=domains_labels,
        n_betas=n_betas,
        distance_metric=distance_metric,
    )

    # if using Wasserstein distance, scale the variation by the original variation so it's hopefully within [0,1]
    if distance_metric == ot.emd2_1d:
        variation_original = model_variation(
            embeddings=originals,
            tasks_labels=tasks_labels,
            domains_labels=domains_labels,
            n_betas=n_betas,
            distance_metric=distance_metric,
        )

    print("Getting reconstruction error...")
    if not isinstance(reconstructions, torch.Tensor):
        reconstructions = torch.Tensor(reconstructions)

    reconstruction_error = torch.nn.MSELoss()(torch.Tensor(originals), reconstructions)
    reconstruction_error_relative = float(
        torch.sqrt(
            reconstruction_error
            / torch.nn.MSELoss()(
                torch.Tensor(originals), torch.zeros_like(torch.Tensor(originals))
            )  # MSELoss with all zeros is equivalent to E(X^2)
        )
    )

    if distance_metric == ot.emd2_1d:
        variation = variation / variation_original

    score = np.mean(accuracies) - variation - reconstruction_error_relative

    return (
        score,
        accuracies,
        variation,
        reconstruction_error_relative,
    )


def variation_different_targets(
    dataset: Dataset,
    domains_labels: list,
    tasks_labels: list,
    model=None,
    n_betas: int = 1000,
    target_label: str = "Machine",
    source_labels: list = [0, 1],
    distance_metric=ot.emd2_1d,
):
    # get model variations for all target dataset sizes
    variations = []
    # initial on source dataset
    x, y = dataset.get_filtered_slice(
        label=target_label,
        label_filter=source_labels,
    )
    variations.append(
        model_variation(
            embeddings=model(torch.Tensor(x.values)).detach().numpy()
            if model
            else x.values,
            tasks_labels=y[tasks_labels].values,
            domains_labels=y[domains_labels].values,
            n_betas=n_betas,
            distance_metric=distance_metric,
        )
    )
    del x, y
    for current_target_label in tqdm(
        set(dataset.Y[target_label].unique()) - set(source_labels)
    ):
        x, y = dataset.get_filtered_slice(
            label=target_label,
            label_filter=source_labels + [current_target_label],
        )
        variations.append(
            model_variation(
                embeddings=model(torch.Tensor(x.values)).detach().numpy()
                if model
                else x.values,
                tasks_labels=y[tasks_labels].values,
                domains_labels=y[domains_labels].values,
                n_betas=n_betas,
                distance_metric=distance_metric,
            )
        )
        del x, y
    return variations


def model_variation(
    embeddings: np.ndarray,
    tasks_labels: np.ndarray,
    domains_labels: np.ndarray,
    n_betas: int = 1000,
    distance_metric=ot.emd2_1d,
    random_state=None,
) -> float:
    # V(model, E) = sup_{beta in S_{d-1}} V(beta^T h, E)
    # where S_{d-1} is the set of all unit vectors in R^d
    # and h is the vector of features (latent space vector)

    assert len(domains_labels.shape) == 2, "domains_labels must be a 2D array"
    assert len(tasks_labels.shape) == 2, "tasks_labels must be a 2D array"

    # generate unit vectors
    betas = generate_unit_vectors(
        n_samples=n_betas, dims=embeddings.shape[1], random_state=random_state
    )

    # calculate V(beta^T h, E) for each beta
    # variations = np.zeros(n_betas)

    def process(i, beta):
        return feature_variation(
            data_of_feature=embeddings @ beta,
            tasks_labels=tasks_labels,
            domains_labels=domains_labels,
            distance_metric=distance_metric,
        )

    variations = Parallel(n_jobs=-1)(
        delayed(process)(i, beta) for i, beta in enumerate(betas)
    )
    # for i, beta in enumerate(betas):
    #     variations[i] = feature_variation(
    #         data_of_feature=embeddings @ beta,
    #         tasks_labels=tasks_labels,
    #         domains_labels=domains_labels,
    #         distance_metric=distance_metric,
    #     )

    return np.max(variations)


def invariant_causal_prediction(
    embeddings: np.ndarray,
    task_labels: np.ndarray,
    domain_labels: np.ndarray,
    source_labels: list,
    target_label,
) -> float:
    # The ICP distance can be computed by training classifiers
    # on the latent representations of two domains, and then
    # computing the difference between their accuracies on the test target domain.

    x_testtarget = embeddings[domain_labels == (target_label + 1)]
    y_testtarget = task_labels[domain_labels == (target_label + 1)]
    if len(x_testtarget) == 0:
        return 1.0

    x_source = embeddings[np.in1d(domain_labels, source_labels)]
    y_source = task_labels[np.in1d(domain_labels, source_labels)]

    classer_source = xgb.XGBClassifier(use_label_encoder=False, verbosity=0).fit(
        x_source, y_source
    )

    x_target = embeddings[domain_labels == target_label]
    y_target = task_labels[domain_labels == target_label]
    classer_target = xgb.XGBClassifier(use_label_encoder=False, verbosity=0).fit(
        x_target, y_target
    )

    return np.abs(
        classer_source.score(x_testtarget, y_testtarget)
        - classer_target.score(x_testtarget, y_testtarget)
    )


def feature_variation(
    data_of_feature: npt.ArrayLike,
    tasks_labels: npt.ArrayLike,
    domains_labels: npt.ArrayLike,
    distance_metric=ot.emd2_1d,
) -> float:
    task_domain_combinations = list(itertools.product(tasks_labels.T, domains_labels.T))
    max_variation = float("-inf")
    for task_col, domain_col in task_domain_combinations:
        variation = feature_variation_task_domain(
            data_of_feature, task_col, domain_col, distance_metric
        )
        max_variation = max(variation, max_variation)
    return max_variation


def feature_variation_task_domain(
    data_of_feature: npt.ArrayLike,
    task_labels: npt.ArrayLike,
    domain_labels: npt.ArrayLike,
    distance_metric=ot.emd2_1d,
) -> float:
    # V(feature, E) = max_{y in Y} sup_{e, e' in E} wasserstein(P(phi_e|y), P(phi_e'|y))
    # where P(phi_e|y) is the conditional distribution of feature phi_e given y

    # normalise data_of_feature using z-score
    mean = np.mean(data_of_feature)
    std = np.std(data_of_feature) if np.std(data_of_feature) > 0 else 1
    data_of_feature = (data_of_feature - mean) / std

    # get all P(phi_e|y)
    y_unique = np.unique(task_labels)
    e_unique = np.unique(domain_labels)

    P_phi = {}

    for y in y_unique:
        for e in e_unique:
            mask = np.isin(task_labels, y) & np.isin(domain_labels, e)
            if y not in P_phi:
                P_phi[y] = {}
            P_phi[y][e] = data_of_feature[mask]

    # calculate distance between each pair of P(phi_e|y) using the desired metric and get max
    combinations = list(itertools.product(y_unique, e_unique, e_unique))
    max_distance = float("-inf")
    for y, e, e_prime in combinations:
        if e != e_prime:
            if len(P_phi[y][e]) <= 10 or len(P_phi[y][e_prime]) <= 10:
                continue
            distance = distance_metric(
                P_phi[y][e] + np.random.normal(0, 0.001, len(P_phi[y][e])),
                P_phi[y][e_prime] + np.random.normal(0, 0.001, len(P_phi[y][e_prime])),
            )
            max_distance = max(distance, max_distance)

    return max_distance


def total_variation_prob_distance(
    p: npt.ArrayLike, q: npt.ArrayLike, summing=True, nsteps: int = 200
) -> float:
    """Calculate total variation distance between two probability distributions
    Formula: TV(P, Q) = 1/2 int_x |P(x) - Q(x)| dx

    The probability distributions are estimated using a Gaussian kernel density estimator from discrete p, q
    """

    # get KDE estimates of P and Q
    kde_p = gaussian_kde(p)
    kde_q = gaussian_kde(q)

    # get min and max of x
    x_min = min(np.min(p), np.min(q))
    x_max = max(np.max(p), np.max(q))

    if summing:
        # sum over points instead of integrating
        integral = np.sum(
            np.abs(
                kde_p(np.linspace(x_min, x_max, nsteps))
                - kde_q(np.linspace(x_min, x_max, nsteps))
            )
            * (x_max - x_min)
            / nsteps
        )

    else:
        # integrate over KDEs
        integral = quad(
            lambda x: np.abs(kde_p(x) - kde_q(x)),
            x_min,
            x_max,
        )[0]

    return integral / 2


def jensen_shannon_divergence_1d(
    p: npt.ArrayLike, q: npt.ArrayLike, nsteps: int = 200
) -> float:
    assert (len(p.shape) == 1) and (len(q.shape) == 1), "p and q must be 1D arrays"

    kde_p = gaussian_kde(p)
    kde_q = gaussian_kde(q)

    x_min = min(np.min(p), np.min(q))
    x_max = max(np.max(p), np.max(q))
    x = np.linspace(x_min, x_max, nsteps)
    # steplength = (x_max - x_min) / nsteps

    p = kde_p(x)
    p[np.isclose(p, 0)] = 0
    q = kde_q(x)
    q[np.isclose(q, 0)] = 0

    js = (jensenshannon(p, q, base=2)) ** 2

    if (js < 0) or (js > 1):
        print(f"Warning: js = {js} is not in [0, 1]")
        print(f"p: {p}")
        print(f"q: {q}")
    return js


def fast_jensen_shannon_divergence_1d(p: npt.ArrayLike, q: npt.ArrayLike):
    """
    Much faster version of the function above. However, approximates the PDFs using histograms instead of KDEs. (might not be as accurate and stable)
    """
    p_values = np.asarray(p)
    q_values = np.asarray(q)

    assert (len(p.shape) == 1) and (len(q.shape) == 1), "p and q must be 1D arrays"

    all_values = np.concatenate((p_values, q_values))
    minimum = np.min(all_values)
    maximum = np.max(all_values)
    bin_range = (minimum, maximum)
    _, bins_arr = np.histogram(all_values, bins="auto", range=bin_range)

    # approximate the probability distributions using histograms
    p_hist, _ = np.histogram(p_values, bins=bins_arr, range=bin_range)
    q_hist, _ = np.histogram(q_values, bins=bins_arr, range=bin_range)

    js = (jensenshannon(p_hist, q_hist, base=2)) ** 2

    # if (js < 0) or (js > 1):
    #     print(f"Warning: js = {js} is not in [0, 1]")
    #     print(f"p: {p}")
    #     print(f"q: {q}")
    return js


def jensen_shannon_distance_1d(
    p: npt.ArrayLike, q: npt.ArrayLike, nsteps: int = 200
) -> float:
    """The probability distributions are estimated using a Gaussian kernel density estimator from discrete p, q"""

    return np.sqrt(jensen_shannon_divergence_1d(p, q, nsteps))


def maximum_mean_discrepancy(
    p, q, kernel=gaussian_kernel, nsamples: int = 200
) -> float:
    """
    Formula: MMD^2(P,Q) = E[k(x,x')] - 2E[k(x,y)] + E[k(y,y')]
    where k is the kernel function.
    """
    assert nsamples > 0, "nsamples must be > 0"

    if (nsamples < len(p)) and (nsamples < len(q)):
        # sample from p and q
        p = np.random.choice(p, nsamples, replace=False)
        q = np.random.choice(q, nsamples, replace=False)

    else:
        print(
            "Warning: nsamples > len(p) or nsamples > len(q). Using all samples instead of sampling."
        )

    kernel_xx = np.array([kernel(x, x_prime) for x, x_prime in itertools.product(p, p)])
    kernel_yy = np.array([kernel(y, y_prime) for y, y_prime in itertools.product(q, q)])
    kernel_xy = np.array([kernel(x, y) for x, y in itertools.product(p, q)])

    mmd_squared = np.mean(kernel_xx) - 2 * np.mean(kernel_xy) + np.mean(kernel_yy)

    return np.sqrt(mmd_squared)


def generate_unit_vectors(n_samples: int, dims: int, random_state=None) -> np.ndarray:
    # generates n_samples unit vectors in dims dimensions
    if random_state is not None:
        np.random.seed(random_state)
    # the first dims number of vectors are the standard basis vectors
    vectors = np.eye(dims)
    if n_samples < dims:
        warnings.warn(
            "n_samples < dims, will still return all standard basis vectors (n_samples = dims))"
        )
    # the remaining vectors are random unit vectors
    if n_samples > dims:
        vectors = np.concatenate(
            (vectors, np.random.normal(size=(n_samples - dims, dims))), axis=0
        )
    return vectors / np.linalg.norm(vectors, axis=1, keepdims=True)
