#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Create toy data

Notes
-----
Generate classification task toy data with artificial domain bias backgrounds
"""

## INIT
import pandas as pd
import numpy as np
import yaml
import sklearn
from sklearn.datasets import make_classification
from sklearn.preprocessing import StandardScaler
import random

from ._math_functions import *


default_random_seed = 12345

## FUNCTIONS


def load_YAML(filepath):
    with open(filepath, "r") as yamlfile:
        config = yaml.safe_load(yamlfile)

    return config


def make_multiclass_classification(
    n_samples=100,
    n_features=20,
    n_classes: list = [2, 2],
    informative_features: list = [[0, 1, 2], [3, 4, 5]],
    redundant_features: int = 0,
    class_sep: list = [1.0, 1.0],
    shuffle=True,
    normalise=False,
    random_state=None,
):
    assert len(n_classes) == len(
        informative_features
    ), "Number of classes must match number of informative features"
    assert len(n_classes) == len(
        class_sep
    ), "Number of classes must match number of class_sep"
    np.random.seed(random_state)
    random.seed(random_state)
    num_categories = len(n_classes)
    datas = []
    for i in range(num_categories):
        X, y = make_classification(
            n_samples=n_samples,
            n_features=n_features,
            n_classes=n_classes[i],
            n_informative=len(informative_features[i]),
            n_redundant=0,
            class_sep=class_sep[i],
            shuffle=False,
            random_state=random_state,
        )

        # swap columns of informative features to their correct positions by iterating backwards
        informative_features[i] = sorted(informative_features[i])
        for j in reversed(range(len(informative_features[i]))):
            X[:, [j, informative_features[i][j]]] = X[
                :, [informative_features[i][j], j]
            ]
        # shuffle the rows
        if shuffle:
            X, y = sklearn.utils.shuffle(X, y)
        datas.append((X, y))

    # sum all X and concatenate all y as multi-class labels
    if len(datas) > 1:
        X = sum([x for x, _ in datas])
        y = np.concatenate([np.reshape(y, (y.shape[0], 1)) for _, y in datas], axis=1)
    else:
        X, y = datas[0][0], np.reshape(datas[0][1], (datas[0][1].shape[0], 1))

    informative_features = set(
        [item for sublist in informative_features for item in sublist]
    )
    # add redundant features
    if redundant_features > 0:
        # add random linear combinations of the informative features to the end of the dataset
        all_features = set(range(X.shape[1]))
        uninformative_features = list(all_features - informative_features)
        assert (
            len(uninformative_features) >= redundant_features
        ), "Not enough uninformative features for selected number of redundant features"
        redundant_features = np.random.choice(
            np.array(uninformative_features), redundant_features, replace=False
        )
        for i in redundant_features:
            # choice = random.choice(["linear", "multiplicative"])
            choice = "linear"  # only linear for now
            if choice == "linear":
                factor_1 = random.uniform(-1, 1)
                factor_2 = random.uniform(-1, 1)
                feature_1 = random.choice(list(informative_features))
                feature_2 = random.choice(
                    [x for x in list(informative_features) if x != feature_1]
                )  # this way, we don't get the same feature twice
                X[:, i] = factor_1 * X[:, feature_1] + factor_2 * X[:, feature_2]
                print(
                    "Added linear redundant feature {}, as {:.2f} * feature {} + {:.2f} * feature {}.".format(
                        i, factor_1, feature_1, factor_2, feature_2
                    )
                )
            elif choice == "multiplicative":
                factor_1 = random.uniform(-1, 1)
                feature_1 = random.choice(list(informative_features))
                feature_2 = random.choice(
                    [x for x in list(informative_features) if x != feature_1]
                )  # this way, we don't get the same feature twice
                exponent = random.choice([-1, 1])
                X[:, i] = factor_1 * X[:, feature_1] * X[:, feature_2] ** exponent
                print(
                    "Added multiplicative redundant feature {}, as {:.2f} * feature {} * feature {} ^ {}".format(
                        i, factor_1, feature_1, feature_2, exponent
                    )
                )
        print("----------------------------------------------------")
        print("Redundant features are:", sorted(redundant_features))

    if normalise:
        scaler = StandardScaler()
        X = scaler.fit_transform(X)

    print("Informative features are:", informative_features)
    return X, y


def label_df(df, domain):
    # generate the main function arguments in the dataframe (labels for affine, continuous for the others)
    if domain["func"] == "affine":
        # make the first rows have all labels in the correct order such that an encode going through
        # row by row will encode Domain 1 as 0, Domain 2 as 1, etc.
        df.loc[range(len(domain["labels"])), domain["name"]] = domain["labels"]
        # after that, distribute the labels randomly uniformly
        df.loc[
            range(len(domain["labels"]), len(df)), domain["name"]
        ] = np.random.choice(
            a=domain["labels"],
            size=len(df) - len(domain["labels"]),
            p=domain["ratios"] / np.sum(domain["ratios"]),
        )
        df.loc[:, domain["name"]] = df.loc[:, domain["name"]].astype(int)
    else:
        df[domain["name"]] = domain["t_max"] * np.random.random_sample(size=len(df))

    return df


def apply_affine(X, y, domain):
    # go through each features and apply the transformation
    dependency = domain["depends_on"]
    if dependency is None:
        for i in domain["labels"]:
            scale = domain["scales"][i]
            shift = domain["shifts"][i]
            for j in domain["features"][i]:
                X[y[domain["name"]] == i, j] = affine(
                    X[y[domain["name"]] == i, j], scale, shift
                )

    elif dependency["type"] == "categorical":
        assert dependency["type"] in [
            "categorical",
            "numeric",
        ], "Invalid dependency type. Must be categorical or numeric."
        depends_labels = [
            int(label) for label in sorted(y[dependency["name"]].unique())
        ]
        for l in depends_labels:
            for i in domain["labels"]:
                scale = (
                    dependency["scales"][l][i]
                    if "scales" in dependency
                    else domain["scales"][i]
                )
                shift = (
                    dependency["shifts"][l][i]
                    if "shifts" in dependency
                    else domain["shifts"][i]
                )
                for j in domain["features"][i]:
                    X[
                        (y[domain["name"]] == i) & (y[dependency["name"]] == l), j
                    ] = affine(
                        X[(y[domain["name"]] == i) & (y[dependency["name"]] == l), j],
                        scale,
                        shift,
                    )

    elif dependency["type"] == "numeric":
        assert dependency["type"] in [
            "categorical",
            "numeric",
        ], "Invalid dependency type. Must be categorical or numeric."
        for i in domain["labels"]:
            scale_func = (
                lambda x: eval(dependency["scales"][i])
                if "scales" in dependency
                else domain["scales"][i]
            )
            shift_func = (
                lambda x: eval(dependency["shifts"][i])
                if "shifts" in dependency
                else domain["shifts"][i]
            )
            for j in domain["features"][i]:
                X[y[domain["name"]] == i, j] = affine(
                    X[y[domain["name"]] == i, j],
                    scale_func(y.loc[y[domain["name"]] == i, dependency["name"]]),
                    shift_func(y.loc[y[domain["name"]] == i, dependency["name"]]),
                )

    return X


def apply_decay(X, y, domain):
    dependency = domain["depends_on"]
    t = y[domain["name"]]
    if dependency is None:
        for i, feat in enumerate(domain["features"]):
            scale = domain["scales"][i]
            rate = domain["rates"][i]
            X[:, feat] = decay(X[:, feat], scale, rate, t)

    elif dependency["type"] == "categorical":
        assert dependency["type"] in [
            "categorical",
            "numeric",
        ], "Invalid dependency type. Must be categorical or numeric."
        depends_labels = [
            int(label) for label in sorted(y[dependency["name"]].unique())
        ]
        for l in depends_labels:
            for i, feat in enumerate(domain["features"]):
                scale = (
                    dependency["scales"][l][i]
                    if "scales" in dependency
                    else domain["scales"][i]
                )
                rate = (
                    dependency["rates"][l][i]
                    if "rates" in dependency
                    else domain["rates"][i]
                )
                t = y.loc[y[dependency["name"]] == l, domain["name"]]
                X[(y[dependency["name"]] == l), feat] = decay(
                    X[(y[dependency["name"]] == l), feat],
                    scale,
                    rate,
                    t,
                )

    elif dependency["type"] == "numeric":
        assert dependency["type"] in [
            "categorical",
            "numeric",
        ], "Invalid dependency type. Must be categorical or numeric."
        for i, feat in enumerate(domain["features"]):
            scale_func = (
                lambda x: eval(dependency["scales"][i])
                if "scales" in dependency
                else domain["scales"][i]
            )
            rate_func = (
                lambda x: eval(dependency["rates"][i])
                if "rates" in dependency
                else domain["rates"][i]
            )
            X[:, feat] = decay(
                X[:, feat],
                scale_func(y.loc[:, dependency["name"]]),
                rate_func(y.loc[:, dependency["name"]]),
                t,
            )

    return X


def apply_sinusoidal(X, y, domain):
    dependency = domain["depends_on"]
    t = y[domain["name"]]
    if dependency is None:
        for i, feat in enumerate(domain["features"]):
            period = domain["periods"][i]
            amplitude = domain["amplitudes"][i]
            phase = domain["phases"][i]
            X[:, feat] = sinusoidal(X[:, feat], period, amplitude, phase, t)

    elif dependency["type"] == "categorical":
        assert dependency["type"] in [
            "categorical",
            "numeric",
        ], "Invalid dependency type. Must be categorical or numeric."
        depends_labels = [
            int(label) for label in sorted(y[dependency["name"]].unique())
        ]
        for l in depends_labels:
            for i, feat in enumerate(domain["features"]):
                period = (
                    dependency["periods"][l][i]
                    if "periods" in dependency
                    else domain["periods"][i]
                )
                amplitude = (
                    dependency["amplitudes"][l][i]
                    if "amplitudes" in dependency
                    else domain["amplitudes"][i]
                )
                phase = (
                    dependency["phases"][l][i]
                    if "phases" in dependency
                    else domain["phases"][i]
                )
                t = y.loc[y[dependency["name"]] == l, domain["name"]]
                X[(y[dependency["name"]] == l), feat] = sinusoidal(
                    X[(y[dependency["name"]] == l), feat],
                    period,
                    amplitude,
                    phase,
                    t,
                )

    elif dependency["type"] == "numeric":
        assert dependency["type"] in [
            "categorical",
            "numeric",
        ], "Invalid dependency type. Must be categorical or numeric."
        for i, feat in enumerate(domain["features"]):
            period_func = (
                lambda x: eval(dependency["periods"][i])
                if "periods" in dependency
                else domain["periods"][i]
            )
            amplitude_func = (
                lambda x: eval(dependency["amplitudes"][i])
                if "amplitudes" in dependency
                else domain["amplitudes"][i]
            )
            phase_func = (
                lambda x: eval(dependency["phases"][i])
                if "phases" in dependency
                else domain["phases"][i]
            )
            X[:, feat] = sinusoidal(
                X[:, feat],
                period_func(y.loc[:, dependency["name"]]),
                amplitude_func(y.loc[:, dependency["name"]]),
                phase_func(y.loc[:, dependency["name"]]),
                t,
            )

    return X


def apply_sawtooth(X, y, domain):
    t = y[domain["name"]]
    dependency = domain["depends_on"]
    if dependency is None:
        for i, feat in enumerate(domain["features"]):
            period = domain["periods"][i]
            amplitude = domain["amplitudes"][i]
            phase = domain["phases"][i]
            X[:, feat] = sawtooth(X[:, feat], period, amplitude, phase, t)

    elif dependency["type"] == "categorical":
        assert dependency["type"] in [
            "categorical",
            "numeric",
        ], "Invalid dependency type. Must be categorical or numeric."
        depends_labels = [
            int(label) for label in sorted(y[dependency["name"]].unique())
        ]
        for l in depends_labels:
            for i, feat in enumerate(domain["features"]):
                period = (
                    dependency["periods"][l][i]
                    if "periods" in dependency
                    else domain["periods"][i]
                )
                amplitude = (
                    dependency["amplitudes"][l][i]
                    if "amplitudes" in dependency
                    else domain["amplitudes"][i]
                )
                phase = (
                    dependency["phases"][l][i]
                    if "phases" in dependency
                    else domain["phases"][i]
                )
                t = y.loc[y[dependency["name"]] == l, domain["name"]]
                X[(y[dependency["name"]] == l), feat] = sawtooth(
                    X[(y[dependency["name"]] == l), feat],
                    period,
                    amplitude,
                    phase,
                    t,
                )

    elif dependency["type"] == "numeric":
        assert dependency["type"] in [
            "categorical",
            "numeric",
        ], "Invalid dependency type. Must be categorical or numeric."
        for i, feat in enumerate(domain["features"]):
            period_func = (
                lambda x: eval(dependency["periods"][i])
                if "periods" in dependency
                else domain["periods"][i]
            )
            amplitude_func = (
                lambda x: eval(dependency["amplitudes"][i])
                if "amplitudes" in dependency
                else domain["amplitudes"][i]
            )
            phase_func = (
                lambda x: eval(dependency["phases"][i])
                if "phases" in dependency
                else domain["phases"][i]
            )
            X[:, feat] = sawtooth(
                X[:, feat],
                period_func(y.loc[:, dependency["name"]]),
                amplitude_func(y.loc[:, dependency["name"]]),
                phase_func(y.loc[:, dependency["name"]]),
                t,
            )

    return X


def apply_slopey_sigmoid(X, y, domain):
    dependency = domain["depends_on"]
    t = y[domain["name"]]
    if dependency is None:
        for i, feat in enumerate(domain["features"]):
            scale = domain["scales"][i]
            rate = domain["rates"][i]
            mu = domain["mus"][i]
            grad = domain["grads"][i]
            X[:, feat] = slopey_sigmoid(X[:, feat], t, scale, rate, mu, grad)

    elif dependency["type"] == "categorical":
        assert dependency["type"] in [
            "categorical",
            "numeric",
        ], "Invalid dependency type. Must be categorical or numeric."
        depends_labels = [
            int(label) for label in sorted(y[dependency["name"]].unique())
        ]
        for l in depends_labels:
            for i, feat in enumerate(domain["features"]):
                scale = (
                    dependency["scales"][l][i]
                    if "scales" in dependency
                    else domain["scales"][i]
                )
                rate = (
                    dependency["rates"][l][i]
                    if "rates" in dependency
                    else domain["rates"][i]
                )
                mu = (
                    dependency["mus"][l][i] if "mus" in dependency else domain["mus"][i]
                )
                grad = (
                    dependency["grads"][l][i]
                    if "grads" in dependency
                    else domain["grads"][i]
                )
                t = y.loc[y[dependency["name"]] == l, domain["name"]]
                X[(y[dependency["name"]] == l), feat] = slopey_sigmoid(
                    X[(y[dependency["name"]] == l), feat], t, scale, rate, mu, grad
                )

    elif dependency["type"] == "numeric":
        assert dependency["type"] in [
            "categorical",
            "numeric",
        ], "Invalid dependency type. Must be categorical or numeric."
        for i, feat in enumerate(domain["features"]):
            scale_func = (
                lambda x: eval(dependency["scales"][i])
                if "scales" in dependency
                else domain["scales"][i]
            )
            rate_func = (
                lambda x: eval(dependency["rates"][i])
                if "rates" in dependency
                else domain["rates"][i]
            )
            mu_func = (
                lambda x: eval(dependency["mus"][i])
                if "mus" in dependency
                else domain["mus"][i]
            )
            grad_func = (
                lambda x: eval(dependency["grads"][i])
                if "grads" in dependency
                else domain["grads"][i]
            )
            X[:, feat] = slopey_sigmoid(
                X[:, feat],
                t,
                scale_func(y.loc[:, dependency["name"]]),
                rate_func(y.loc[:, dependency["name"]]),
                mu_func(y.loc[:, dependency["name"]]),
                grad_func(y.loc[:, dependency["name"]]),
            )


## MAIN DOMAIN BIAS FUNCTION

# dictionary mapping function names to the correct apply function
transformation_functions = {
    "affine": apply_affine,
    "sinusoidal": apply_sinusoidal,
    "decay": apply_decay,
    "sawtooth": apply_sawtooth,
    "sigmoid": apply_slopey_sigmoid,
}


def add_domain_bias(X, y, config):
    X = X.copy()
    y = y.copy()
    domains = config["domains"]
    # go through all domains and apply transformations
    df_temp = pd.DataFrame()
    for i in range(y.shape[1]):
        df_temp[f"ClassCategory_{i}"] = y[:, i]
    y = df_temp
    still_to_process = [domain for domain in domains]
    while len(still_to_process) > 0:
        for domain in still_to_process:
            start_process_len = len(still_to_process)
            if (
                domain["depends_on"] is None
                or domain["depends_on"]["name"] in y.columns
            ):
                print(
                    "Adding bias transform {}, depending on {}".format(
                        domain["name"],
                        domain["depends_on"]["name"]
                        if isinstance(domain["depends_on"], dict)
                        else None,
                    )
                )
                # check if "name" is already in y otherwise create the column and fill with labels
                if domain["name"] not in y.columns:
                    y[domain["name"]] = np.zeros(len(y))
                    y = label_df(y, domain)

                # apply the transformation to the data
                X = transformation_functions[domain["func"]](X, y, domain)
                # remove the domain from the list of domains to process
                still_to_process.remove(domain)
        assert (
            len(still_to_process) != start_process_len
        ), "Cycle in domain dependencies"

    return X, y
