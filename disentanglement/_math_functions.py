#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from scipy import signal
import itertools


def affine(x, scale, shift):
    """
    Apply an affine transform to an array
    """
    return x * scale + shift


def decay(x, scale, rate, t):
    """
    Apply an exponential decay to an array with time t
    """
    return x - scale * (1 - np.exp(-rate * t))


def sinusoidal(x, period, amplitude, phase, t):
    """
    Apply a sinusoidal fluctuation to an array with time t
    """
    return x + amplitude * np.sin(2 * np.pi / period * t + phase)


def sawtooth(x, period, amplitude, phase, t):
    """
    Apply a sawtooth fluctuation to an array with time t
    """
    return x + amplitude * signal.sawtooth(2 * np.pi / period * t + phase)


def slopey_sigmoid(x, t, scale, rate, mu, grad):
    """
    Apply a slopey sigmoid function to an array with time t
    """
    return x + scale / (1 + np.exp(rate * (t - mu))) + grad * t


def make_step_function(labels, scales, shifts):
    """
    Make a function that applies affine by label
    (effectively a step function with scaling)

    --> can take in x and label as arrays
    """
    funcs = []
    for i in labels:
        funcs.append(lambda x: affine(x, scales[i], shifts[i]))
    return np.vectorize(lambda x, label: funcs[int(label)](x))


def cartesian_product(arrays: list):
    return np.array(list(itertools.product(*arrays)))


def gaussian_kernel(x, y, sigma: float = 1.0):
    return np.exp(-np.linalg.norm(x - y) ** 2 / (2 * sigma**2))
