######################
# INIT
######################

import warnings

import numpy as np
import pandas as pd
import sklearn
import torch
from scipy.stats import median_abs_deviation
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from torch import nn

random_state = 12345


######################
# Loading Data
######################


class Dataset:
    def __init__(self, data, X_labels: list, Y_labels: list, parse_dates=None):
        assert isinstance(
            data, (pd.DataFrame, str)
        ), "data must be a pandas DataFrame or path to csv"
        if isinstance(data, str):
            self.data = pd.read_csv(data, parse_dates=parse_dates)
        else:
            self.data = data.copy()

        self.X_labels = X_labels
        self.Y_labels = Y_labels
        self.X = self.data.loc[:, self.X_labels]
        self.Y = self.data.loc[:, self.Y_labels]
        self._is_normalised = False

    def make_train_splits(
        self, label=None, label_filter=None, batch_size=10000, random_state=random_state
    ):
        # filters for the domains used in training then train/test/val split
        if self._is_normalised is False:
            warnings.warn("Data not yet normalised.")
        if label != None:
            split = self.Y[label].isin(label_filter).values
            Y = self.Y[split]
            X = self.X[split]
        else:
            Y = self.Y
            X = self.X
        train_split, val_split, test_split = get_train_splits(
            X.index, random_state=random_state
        )
        self.X_train = X.loc[train_split]
        self.Y_train = Y.loc[train_split]
        self.train_dataloader = create_dataloader(
            self.X_train, self.Y_train, batch_size
        )
        self.train_dataloader_unbatched = create_dataloader(self.X_train, self.Y_train)
        self.X_val = X.loc[val_split]
        self.Y_val = Y.loc[val_split]
        self.val_dataloader = create_dataloader(self.X_val, self.Y_val)
        self.X_test = X.loc[test_split]
        self.Y_test = Y.loc[test_split]
        self.test_dataloader = create_dataloader(self.X_test, self.Y_test)

    def normalise(self, on=None, y_columns=None):
        # normalise self.X to 0 mean and 1 std per column
        # returns scaler as well for later inverse_transform
        # can work per label if given "on"
        if on is not None:
            assert on in list(self.Y), f"{on} not in Y columns"
            scaler = []  # scaler contains all scalers per label
            labels = self.Y[on].unique()
            for i, label in enumerate(labels):
                scaler.append(
                    preprocessing.StandardScaler().fit(
                        self.X.loc[self.Y[on] == label, :]
                    )
                )
                self.X.loc[self.Y[on] == label, :] = scaler[i].transform(
                    self.X.loc[self.Y[on] == label, :]
                )
        else:  # if no "on" then just normalise on all
            scaler = preprocessing.StandardScaler()
            scaler.fit(self.X)
            self.X.loc[:] = scaler.transform(self.X)
        self.scalers = scaler
        self._is_normalised = True

        if y_columns is not None:
            for col in y_columns:
                self.Y.loc[:, col] = self.Y[col] / np.max(self.Y[col])

    def shuffle(self):
        # shuffle rows of X and Y
        X, Y = sklearn.utils.shuffle(self.X, self.Y)
        self.X = X.reset_index(drop=True)
        self.Y = Y.reset_index(drop=True)

    def get_filtered_slice(self, label, label_filter):
        # returns a slice of the dataset with only the given label_filter
        assert label in self.Y.columns
        for key in label_filter:
            assert key in self.Y[label].unique()
        split = self.Y[label].isin(label_filter).values
        Y = self.Y[split]
        X = self.X[split]
        return X, Y

    def sample(self, n_samples: int, balance_on=None, random_state=None):
        # returns a random sample of n_samples from the dataset
        # if balance_on is not None then the samples will be balanced on the class labels in that column
        if balance_on is not None:
            assert balance_on in self.Y.columns
            # get samppling indexes, same number of each class
            indexes = []
            for label in self.Y[balance_on].unique():
                indexes.append(
                    self.Y[self.Y[balance_on] == label]
                    .sample(
                        n=n_samples // len(self.Y[balance_on].unique()),
                        random_state=random_state,
                    )
                    .index
                )
            indexes = np.concatenate(indexes)
            # shuffle indexes
            np.random.shuffle(indexes)
        else:
            indexes = self.X.sample(n=n_samples, random_state=random_state).index
        self.X = self.X.loc[indexes]
        self.Y = self.Y.loc[indexes]

    # def MakeExternalValidationsplits


#! below two only work for X,Y as pandas DataFrames
class CustomDataset(torch.utils.data.Dataset):
    def __init__(self, X, Y):
        self.X = torch.from_numpy(X.values).float()
        self.Y = []
        for col in Y:
            y = torch.from_numpy(Y[col].values).long()
            self.Y.append(y)

    def __getitem__(self, idx):
        X = self.X[idx]
        Y = [y[idx] for y in self.Y]
        return X, Y

    def __len__(self):
        return len(self.X)


def create_dataloader(X, Y, batch_size=None, shuffle=True, num_dataloader_workers=0):
    dataset = CustomDataset(X, Y)
    if batch_size == None:
        batch_size = len(dataset)
        shuffle = False
    dataloader = torch.utils.data.DataLoader(
        dataset,
        shuffle=shuffle,
        batch_size=batch_size,
        num_workers=num_dataloader_workers,
        # pin_memory=True,
    )
    return dataloader


def get_train_splits(index, train_size=0.7, val_size=0.1, random_state=random_state):
    # default gives 0.7,0.15,0.15
    train_index, test_index = train_test_split(
        index, train_size=train_size, shuffle=True, random_state=random_state
    )
    val_size = val_size / (1 - train_size)
    test_index, val_index = train_test_split(
        test_index,
        test_size=val_size,
        shuffle=True,
        random_state=random_state,
    )
    return train_index, val_index, test_index


######################
# Preprocessing
######################


def get_data_centre_using_MAD(X, y, thresh=3.5, return_mask=False):
    low = np.median(y, axis=0) - thresh * median_abs_deviation(y)
    high = np.median(y, axis=0) + thresh * median_abs_deviation(y)
    # create mask such that y is between low and high in all dimensions
    mask = np.ones(len(y), dtype=bool)
    for i in range(len(y[0])):
        mask = mask & (y[:, i] >= low[i]) & (y[:, i] <= high[i])
    if return_mask:  # note: X technically not important in this case
        return mask
    return X[mask], y[mask]


######################
# Training
######################


# Gradient Reversal Layer implementation from https://github.com/tadeephuy/GradientReversal
class GradientReversal_function(torch.autograd.Function):
    @staticmethod
    def forward(ctx, x, alpha):
        ctx.save_for_backward(x, alpha)
        return x

    @staticmethod
    def backward(ctx, grad_output):
        grad_input = None
        _, alpha = ctx.saved_tensors
        if ctx.needs_input_grad[0]:
            grad_input = -alpha * grad_output
        return grad_input, None


revgrad = GradientReversal_function.apply


class GradientReversal(nn.Module):
    def __init__(self, alpha=1.0):
        super().__init__()
        self.alpha = torch.tensor(alpha, requires_grad=False)

    def forward(self, x):
        return revgrad(x, self.alpha)
