#!/usr/bin/env python3
# -*- coding: utf-8 -*-

######################
# INIT
######################

import pandas as pd
from sklearn import preprocessing

######################
# FUNCTIONS
######################


def multiencode(df):
    columns = list(df.columns)
    y = df.values
    multienc = MultiEncoder(y)
    y = multienc.transform().astype(float)
    return pd.DataFrame(y, columns=columns), multienc


# check if python is executing from a notebook:
def is_notebook() -> bool:
    try:
        shell = get_ipython().__class__.__name__
        if shell == "ZMQInteractiveShell":
            return True  # Jupyter notebook or qtconsole
        elif shell == "TerminalInteractiveShell":
            return False  # Terminal running IPython
        else:
            return False  # Other type (?)
    except NameError:
        return False  # Probably standard Python interpreter


######################
# CLASSES
######################


# class to use label encoder on multiple columns
# also checks if column contains float or int before encoding
class MultiEncoder:  # really shouldn't include array as standard but in a "fit" method.....
    def __init__(self, arr) -> None:
        self.class_data = arr
        assert len(arr.shape) <= 2, "only 1- or 2-dimensional arrays supported"
        # for 1d-array
        if len(arr.shape) == 1:
            try:
                float(arr[0])  #! only checks first entry, not entire array
                print("Note: no transformation needed (already numeric type)")
                self.transformed = arr
            except ValueError:
                le = preprocessing.LabelEncoder()
                le.fit_transform(arr)
                self.transformed = arr
                self.encoders = le

        # for 2d-array
        if len(arr.shape) == 2:
            self.encoders = []
            for i in range(arr.shape[1]):
                try:
                    float(arr[0, i])
                    self.encoders.append(0)
                except ValueError:
                    self.encoders.append(preprocessing.LabelEncoder().fit(arr[:, i]))

    # do the transform
    def transform(self):
        arr = self.class_data
        if len(arr.shape) == 1:
            return self.transformed
        if len(arr.shape) == 2:
            for i in range(arr.shape[1]):
                if self.encoders[i] == 0:
                    pass
                else:
                    arr[:, i] = self.encoders[i].transform(arr[:, i])
            self.transformed = arr
        return self.transformed

    # do the inverse_transform
    def inverse_transform(self):
        assert hasattr(self, "transformed"), "Nothing to inverse transform!"
        arr = self.transformed
        if len(arr.shape) == 1:
            return self.encoders.inverse_transform(arr)
        if len(arr.shape) == 2:
            for i in range(arr.shape[1]):
                if self.encoders[i] == 0:
                    pass
                else:
                    arr[:, i] = self.encoders[i].inverse_transform(
                        arr[:, i].astype(int)
                    )
            self.inv_transformed = arr
        return self.inv_transformed
