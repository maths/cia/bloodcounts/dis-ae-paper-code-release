#!/usr/bin/env python
# coding: utf-8
import sys

sys.path.append("../")

import pandas as pd
import numpy as np
import sklearn

from disentanglement.toydata import (
    add_domain_bias,
    load_YAML,
    make_multiclass_classification,
)

# import config file for dataset specifications
config = load_YAML("../config-files/dataset_5affine_decay_periodic_multiclass.yaml")

X, y = sklearn.datasets.make_multilabel_classification(
    n_samples=26_000,
    n_features=32,
    n_classes=3,
    n_labels=2,
    random_state=12345,
)

# make X positive by adding 20
X = X + 20.0

X, y = add_domain_bias(X, y, config)

df = pd.DataFrame(X)
dataset = pd.concat([df, y], axis=1)

bins = np.linspace(0, y["VenepunctureDelay"].max(), 11)
labels = np.arange(len(bins) - 1)
dataset["vendelay_binned"] = pd.cut(
    dataset["VenepunctureDelay"], bins=bins, labels=labels
)

bins = np.linspace(0, y["TimeIntoStudy"].max(), 11)
labels = np.arange(len(bins) - 1)
dataset["studytime_binned"] = pd.cut(dataset["TimeIntoStudy"], bins=bins, labels=labels)

dataset.to_csv("../datasets/data_C.csv", index=False)
