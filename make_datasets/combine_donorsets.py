#!/usr/bin/env python
# coding: utf-8
import sys

sys.path.append("../")


import numpy as np
import pandas as pd

from disentanglement.training_utils import get_data_centre_using_MAD

feats_bl = [
    "WBC_10_9_L_bl",
    "RBC_10_12_L_bl",
    "HGB_g_dL_bl",
    "HCT_PCT_bl",
    "MCV_fL_bl",
    "MCH_pg_bl",
    "MCHC_g_dL_bl",
    "PLT_10_9_L_bl",
    "RDW_SD_fL_bl",
    "RDW_CV_PCT_bl",
    "PDW_fL_bl",
    "MPV_fL_bl",
    "P_LCR_PCT_bl",
    "PCT_PCT_bl",
    #     "NRBC_10_9_L_bl",
    #     "NRBC_PCT_bl",
    "NEUT_10_9_L_bl",
    "LYMPH_10_9_L_bl",
    "MONO_10_9_L_bl",
    "EO_10_9_L_bl",
    "BASO_10_9_L_bl",
    "NEUT_PCT_bl",
    "LYMPH_PCT_bl",
    "MONO_PCT_bl",
    "EO_PCT_bl",
    "BASO_PCT_bl",
    "IG_10_9_L_bl",
    "IG_PCT_bl",
    "RET_PCT_bl",
    "RET_10_6_uL_bl",
    "IRF_PCT_bl",
    "LFR_PCT_bl",
    "MFR_PCT_bl",
    "HFR_PCT_bl",
    "RET_He_pg_bl",
    "IPF_bl",
    "PLT_I_10_9_L_bl",
    "MicroR_PCT_bl",
    "MacroR_PCT_bl",
    "TNC_10_9_L_bl",
    "WBC_N_10_9_L_bl",
    "TNC_N_10_9_L_bl",
    "BA_N_10_9_L_bl",
    "BA_N_PCT_bl",
    "WBC_D_10_9_L_bl",
    "TNC_D_10_9_L_bl",
    "NEUTx_10_9_L_bl",
    "NEUTx_PCT_bl",
    "LYMP_10_9_L_bl",
    "LYMP_PCT_bl",
    #     "HFLC_10_9_L_bl",
    #     "HFLC_PCT_bl",
    "BA_D_10_9_L_bl",
    "BA_D_PCT_bl",
    "NE_SSC_ch_bl",
    "NE_SFL_ch_bl",
    "NE_FSC_ch_bl",
    "LY_X_ch_bl",
    "LY_Y_ch_bl",
    "LY_Z_ch_bl",
    "MO_X_ch_bl",
    "MO_Y_ch_bl",
    "MO_Z_ch_bl",
    "NE_WX_bl",
    "NE_WY_bl",
    "NE_WZ_bl",
    "LY_WX_bl",
    "LY_WY_bl",
    "LY_WZ_bl",
    "MO_WX_bl",
    "MO_WY_bl",
    "MO_WZ_bl",
    "RBC_O_10_12_L_bl",
    "PLT_O_10_9_L_bl",
    "RBC_He_pg_bl",
    "Delta_He_pg_bl",
    "RET_Y_ch_bl",
    "RET_RBC_Y_ch_bl",
    "IRF_Y_ch_bl",
    #     "FRC_10_12_L_bl",
    #     "FRC_PCT_bl",
    "HYPO_He_PCT_bl",
    "HYPER_He_PCT_bl",
    "RPI_bl",
    "RET_UPP_bl",
    "RET_TNC_bl",
    "PLT_F_10_9_L_bl",
    "H_IPF_bl",
    "IPFx_10_9_L_bl",
]
feats_rich = []
for feature in feats_bl:
    if feature[-2:] == "bl":
        feats_rich.append(feature[:-3])


INTERVAL = pd.read_csv(
    "../datasets/INTERVAL_rich_filtered.csv", parse_dates=["datetime_ven"]
)
INTERVAL.head()


INTERVAL.rename(columns={"LY_WZ_bl_x": "LY_WZ_bl"}, inplace=True)


for column in INTERVAL.columns:
    # remove the _bl if it ends in _bl
    if column[-2:] == "bl":
        INTERVAL.rename(columns={column: column[:-3]}, inplace=True)


COMPARE = pd.read_csv(
    "../datasets/COMPARE_rich_filtered.csv", parse_dates=["datetime_ven"]
)
COMPARE.head()


COMPARE["Analyser ID"] = "XN-COMPARE"


INTERVAL["Visit"] = "Visit 1"

donorset = pd.concat([INTERVAL, COMPARE])
donorset.head()


donorset.drop(
    columns=[
        "AppointmentTime",
        "AppointmentDate",
        "ProcessDate",
        "ProcessTime",
        "nDonation_hx2",
        "nDonation_hx5",
    ],
    inplace=True,
)


replacement_mapping = {
    "XN-10^11036": "INTERVAL 1",
    "XN-10^11041": "INTERVAL 2",
    "XN-COMPARE": "COMPARE",
}

# Replace the values in the "Analyser ID" column
donorset["Analyser ID"] = donorset["Analyser ID"].replace(replacement_mapping)


donorset = donorset[(donorset["Height"] < 2.2) & (donorset["Height"] > 1.0)]
donorset = donorset[(donorset["Weight"] < 200.0) & (donorset["Weight"] > 45.0)]

bins = np.linspace(0, 24, 11)
labels = np.arange(len(bins) - 1)
donorset["t_day"] = pd.cut(
    donorset.loc[:, "t_day / hours"], bins=bins, labels=labels
)  # ended up not getting used

bins = [0, 8, 16, 24, 32, float("inf")]
labels = ["0-8h", "8-16h", "16-24h", "24-32h", "32-36h"]
donorset["vendelay"] = pd.cut(
    donorset.loc[:, "t_ven / hours"], bins=bins, labels=labels
)

donorset["season"] = donorset["datetime_ven"].dt.month

bins = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, float("inf")]
# bins = np.array([0., 15., 25., 65., 200])
labels = [
    "0-10",
    "10-20",
    "20-30",
    "30-40",
    "40-50",
    "50-60",
    "60-70",
    "70-80",
    "80-90",
    "90-100",
    "100+",
]
donorset["age_bracket"] = pd.cut(donorset.loc[:, "Age"], bins=bins, labels=labels)

donorset["BMI"] = donorset["Weight"] / (donorset["Height"] ** 2)

donorset = donorset[~(donorset["BMI"] > 60)]

bins = np.array([0.0, 18.5, 24.9, 29.9, 100])
labels = ["<18.5", "18.5-25", "25-30", ">30"]
# labels = np.arange(len(bins)-1)
donorset["bmi_bracket"] = pd.cut(donorset.loc[:, "BMI"], bins=bins, labels=labels)

donorset = donorset[
    get_data_centre_using_MAD(
        X=None,
        y=donorset[["Height", "Weight", "Age"]].values,
        return_mask=True,
    )
]

donorset = donorset[
    get_data_centre_using_MAD(
        X=None,
        y=donorset[["RPI", "IG_10_9_L"]].values,
        return_mask=True,
        thresh=4.0,
    )
]  # a lot of outliers on these measurements

donorset = donorset[donorset["Visit"] == "Visit 1"]

donorset.to_csv("../datasets/FBC_donordata.csv")
