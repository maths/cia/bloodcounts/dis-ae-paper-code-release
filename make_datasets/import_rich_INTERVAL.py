#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###########################
# IMPORT MODULES
###########################

import numpy as np
import pandas as pd
from datetime import datetime
import argparse

###########################
# COLUMNS
###########################

feats_bl = [
    "WBC_10_9_L_bl",
    "RBC_10_12_L_bl",
    "HGB_g_dL_bl",
    "HCT_PCT_bl",
    "MCV_fL_bl",
    "MCH_pg_bl",
    "MCHC_g_dL_bl",
    "PLT_10_9_L_bl",
    "RDW_SD_fL_bl",
    "RDW_CV_PCT_bl",
    "PDW_fL_bl",
    "MPV_fL_bl",
    "P_LCR_PCT_bl",
    "PCT_PCT_bl",
    # "NRBC_10_9_L_bl",
    # "NRBC_PCT_bl",
    "NEUT_10_9_L_bl",
    "LYMPH_10_9_L_bl",
    "MONO_10_9_L_bl",
    "EO_10_9_L_bl",
    "BASO_10_9_L_bl",
    "NEUT_PCT_bl",
    "LYMPH_PCT_bl",
    "MONO_PCT_bl",
    "EO_PCT_bl",
    "BASO_PCT_bl",
    "IG_10_9_L_bl",
    "IG_PCT_bl",
    "RET_PCT_bl",
    "RET_10_6_uL_bl",
    "IRF_PCT_bl",
    "LFR_PCT_bl",
    "MFR_PCT_bl",
    "HFR_PCT_bl",
    "RET_He_pg_bl",
    "IPF_bl",
    "PLT_I_10_9_L_bl",
    "MicroR_PCT_bl",
    "MacroR_PCT_bl",
    "TNC_10_9_L_bl",
    "WBC_N_10_9_L_bl",
    "TNC_N_10_9_L_bl",
    "BA_N_10_9_L_bl",
    "BA_N_PCT_bl",
    "WBC_D_10_9_L_bl",
    "TNC_D_10_9_L_bl",
    "NEUTx_10_9_L_bl",
    "NEUTx_PCT_bl",
    "LYMP_10_9_L_bl",
    "LYMP_PCT_bl",
    # "HFLC_10_9_L_bl",
    # "HFLC_PCT_bl",
    "BA_D_10_9_L_bl",
    "BA_D_PCT_bl",
    "NE_SSC_ch_bl",
    "NE_SFL_ch_bl",
    "NE_FSC_ch_bl",
    "LY_X_ch_bl",
    "LY_Y_ch_bl",
    "LY_Z_ch_bl",
    "MO_X_ch_bl",
    "MO_Y_ch_bl",
    "MO_Z_ch_bl",
    "NE_WX_bl",
    "NE_WY_bl",
    "NE_WZ_bl",
    "LY_WX_bl",
    "LY_WY_bl",
    "LY_WZ_bl_x",
    "MO_WX_bl",
    "MO_WY_bl",
    "MO_WZ_bl",
    "RBC_O_10_12_L_bl",
    "PLT_O_10_9_L_bl",
    "RBC_He_pg_bl",
    "Delta_He_pg_bl",
    "RET_Y_ch_bl",
    "RET_RBC_Y_ch_bl",
    "IRF_Y_ch_bl",
    # "FRC_10_12_L_bl",
    # "FRC_PCT_bl",
    "HYPO_He_PCT_bl",
    "HYPER_He_PCT_bl",
    "RPI_bl",
    "RET_UPP_bl",
    "RET_TNC_bl",
    "PLT_F_10_9_L_bl",
    "H_IPF_bl",
    "IPFx_10_9_L_bl",
]

###########################
# FUNCTIONS
###########################


###########################
# MAIN SCRIPT
###########################

if __name__ == "__main__":
    parser = argparse.ArgumentParser("Handle input and output files.")
    parser.add_argument(
        "-in_file",
        help="location of input file",
        default="../study_data/INTERVALdata_14MAY2020.csv",
        type=str,
    )
    parser.add_argument(
        "-out_file",
        help="path to outputfile to be generated",
        default="../processed_data/INTERVAL_filtered.csv",
        type=str,
    )
    parser.add_argument(
        "-mach_file",
        help="path to machine information file",
        default="../study_data/epicovid_analyser.csv",
        type=str,
    )

    args = parser.parse_args()
    data_path = args.in_file
    machine_path = args.mach_file

    # import raw INTERVAL
    df_raw = pd.read_csv(data_path, encoding="latin1")
    df_raw.set_index("identifier", inplace=True)
    # dropping all entries for which df_raw has duplicate indices
    df_raw = df_raw[~df_raw.index.duplicated(keep="first")]
    # drop NaNs in times (removes quite a lot of rows! ~9000)
    df_raw.dropna(
        subset=[
            "attendanceDate",
            "appointmentTime",
            "processDate_bl",
            "processTime_bl",
        ],
        inplace=True,
    )

    # import INTERVAL machine association file
    mach = pd.read_csv(machine_path, encoding="latin1")
    mach.rename(columns={"EpiCovId_bl": "identifier"}, inplace=True)
    mach.set_index("identifier", inplace=True)
    mach = mach[~mach.index.duplicated(keep="first")]
    # join into interval FBC file (removes around half the rows as machine is unknown for these)
    df_raw = pd.merge(df_raw, mach, how="inner", on="identifier")

    # building our dataframe piece by piece
    df = pd.DataFrame()

    for feature in feats_bl:
        df[feature] = df_raw[feature]

    # keep only within suggested limits (Vis & Huisman, 2016)
    df = df[df["HGB_g_dL_bl"] >= 7.0]
    df = df[(df["WBC_10_9_L_bl"] >= 2.0) & (df["WBC_10_9_L_bl"] <= 40.0)]
    df = df[df["PLT_F_10_9_L_bl"] >= 50.0]

    df = df[df["NEUT_10_9_L_bl"] >= 0.5]

    # add subject sex as column
    df["Sex"] = df_raw["sexPulse"]
    df.loc[df["Sex"] == 1, "Sex"] = "M"
    df.loc[df["Sex"] == 2, "Sex"] = "F"

    # add subject age as column
    df["Age"] = df_raw["agePulse"]
    # add subject height as column
    df["Height"] = df_raw["ht_bl"]
    # add subject weight as column
    df["Weight"] = df_raw["wt_bl"]
    # add subject ethnicity as column
    df["Ethnicity"] = df_raw["ethnicPulse"]

    # add machine as column
    df["Analyser ID"] = df_raw["Analyzer ID"]

    # add time columns
    df["datetime_ven"] = pd.to_datetime(
        df_raw["attendanceDate"] + df_raw["appointmentTime"], format="%d%b%Y%H:%M"
    )
    df["datetime_process"] = pd.to_datetime(
        df_raw["processDate_bl"] + df_raw["processTime_bl"], format="%d%b%Y%H:%M:%S"
    )
    df["sampleage_h"] = (df["datetime_process"] - df["datetime_ven"]).astype(
        "timedelta64[m]"
    ) / 60.0  # use minute-precision hours as delta

    # set study startdate
    studystart = min(df.loc[:, "datetime_process"]).replace(
        hour=0, minute=0, second=0, microsecond=0
    )

    # get all the time terms for potential domain shift
    df.loc[:, "t / days"] = (df.loc[:, "datetime_process"] - studystart).astype(
        "timedelta64[D]"
    )

    df.loc[:, "t_day / hours"] = (
        df.loc[:, "datetime_process"] - df.loc[:, "datetime_process"].dt.floor("D")
    ).astype(
        "timedelta64[m]"
    ) / 60.0  # use minute-precision hours

    df.loc[:, "t_ven / hours"] = df.loc[:, "sampleage_h"]

    df.loc[:, "t_year / days"] = (
        df.loc[:, "datetime_process"]
        - df.loc[:, "datetime_process"].dt.to_period("Y").dt.to_timestamp()
    ).astype("timedelta64[D]")

    df.loc[:, "d_week"] = df.loc[:, "datetime_process"].dt.weekday

    # drop all rows with sample older than 36h at process
    df = df[df["sampleage_h"] <= 36]
    # drop rows with NaNs
    df.dropna(axis=0, inplace=True)
    # drop all rows with any zeros
    # df = df.loc[~(df == 0).any(axis=1)]
    # drop all entries with MPV > 13 as these are flagged as unreliable by Sysmex instruments (Sysmex only)
    df = df.loc[df.loc[:, "MPV_fL_bl"] <= 13]
    # order by datetime_process
    df = df.sort_values(by="datetime_process")

    # save filtered and cleaned dataframe
    df.to_csv(args.out_file, index=True)
