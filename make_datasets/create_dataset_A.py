#!/usr/bin/env python
# coding: utf-8
import sys

sys.path.append("../")

import pandas as pd

from disentanglement.toydata import (
    add_domain_bias,
    load_YAML,
    make_multiclass_classification,
)

# import config file for dataset specifications
config = load_YAML("../config-files/dataset_5affine.yaml")

# make classification problem without domain biases
# (fancy version of make_classification from sklearn)
X, y = make_multiclass_classification(
    normalise=True,
    random_state=12345,
    n_features=config["class_config"]["n_features"],
    n_samples=config["class_config"]["n_samples"],
    redundant_features=config["class_config"]["n_redundant"],
    informative_features=config["class_config"]["n_informative"],
    n_classes=config["class_config"]["n_classes"],
    class_sep=[1.0],
)

# make X positive by adding 20
X = X + 20.0

X, y = add_domain_bias(X, y, config)

df = pd.DataFrame(X)
dataset = pd.concat([df, y], axis=1)

dataset.to_csv("../datasets/data_A.csv", index=False)
