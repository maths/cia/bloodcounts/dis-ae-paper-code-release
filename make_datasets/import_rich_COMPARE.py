#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###########################
# IMPORT MODULES
###########################


import argparse

import pandas as pd

###########################
# COLUMNS
###########################

feats_v1 = [
    "WBC_10_9_L_v1",
    "RBC_10_12_L_v1",
    "HGB_g_dL_v1",
    "HCT_PCT_v1",
    "MCV_fL_v1",
    "MCH_pg_v1",
    "MCHC_g_dL_v1",
    "PLT_10_9_L_v1",
    "RDW_SD_fL_v1",
    "RDW_CV_PCT_v1",
    "PDW_fL_v1",
    "MPV_fL_v1",
    "P_LCR_PCT_v1",
    "PCT_PCT_v1",
    # "NRBC_10_9_L_v1",
    # "NRBC_PCT_v1",
    "NEUT_10_9_L_v1",
    "LYMPH_10_9_L_v1",
    "MONO_10_9_L_v1",
    "EO_10_9_L_v1",
    "BASO_10_9_L_v1",
    "NEUT_PCT_v1",
    "LYMPH_PCT_v1",
    "MONO_PCT_v1",
    "EO_PCT_v1",
    "BASO_PCT_v1",
    "IG_10_9_L_v1",
    "IG_PCT_v1",
    "RET_PCT_v1",
    "RET_10_6_uL_v1",
    "IRF_PCT_v1",
    "LFR_PCT_v1",
    "MFR_PCT_v1",
    "HFR_PCT_v1",
    "RET_He_pg_v1",
    "IPF_v1",
    "PLT_I_10_9_L_v1",
    "MicroR_PCT_v1",
    "MacroR_PCT_v1",
    "TNC_10_9_L_v1",
    "WBC_N_10_9_L_v1",
    "TNC_N_10_9_L_v1",
    "BA_N_10_9_L_v1",
    "BA_N_PCT_v1",
    "WBC_D_10_9_L_v1",
    "TNC_D_10_9_L_v1",
    "NEUTx_10_9_L_v1",
    "NEUTx_PCT_v1",
    "LYMP_10_9_L_v1",
    "LYMP_PCT_v1",
    # "HFLC_10_9_L_v1",
    # "HFLC_PCT_v1",
    "BA_D_10_9_L_v1",
    "BA_D_PCT_v1",
    "NE_SSC_ch_v1",
    "NE_SFL_ch_v1",
    "NE_FSC_ch_v1",
    "LY_X_ch_v1",
    "LY_Y_ch_v1",
    "LY_Z_ch_v1",
    "MO_X_ch_v1",
    "MO_Y_ch_v1",
    "MO_Z_ch_v1",
    "NE_WX_v1",
    "NE_WY_v1",
    "NE_WZ_v1",
    "LY_WX_v1",
    "LY_WY_v1",
    "LY_WZ_v1",
    "MO_WX_v1",
    "MO_WY_v1",
    "MO_WZ_v1",
    "RBC_O_10_12_L_v1",
    "PLT_O_10_9_L_v1",
    "RBC_He_pg_v1",
    "Delta_He_pg_v1",
    "RET_Y_ch_v1",
    "RET_RBC_Y_ch_v1",
    "IRF_Y_ch_v1",
    # "FRC_10_12_L_v1",
    # "FRC_PCT_v1",
    "HYPO_He_PCT_v1",
    "HYPER_He_PCT_v1",
    "RPI_v1",
    "RET_UPP_v1",
    "RET_TNC_v1",
    "PLT_F_10_9_L_v1",
    "H_IPF_v1",
    "IPFx_10_9_L_v1",
]

feats_v2 = [
    "WBC_10_9_L_v2",
    "RBC_10_12_L_v2",
    "HGB_g_dL_v2",
    "HCT_PCT_v2",
    "MCV_fL_v2",
    "MCH_pg_v2",
    "MCHC_g_dL_v2",
    "PLT_10_9_L_v2",
    "RDW_SD_fL_v2",
    "RDW_CV_PCT_v2",
    "PDW_fL_v2",
    "MPV_fL_v2",
    "P_LCR_PCT_v2",
    "PCT_PCT_v2",
    # "NRBC_10_9_L_v2",
    # "NRBC_PCT_v2",
    "NEUT_10_9_L_v2",
    "LYMPH_10_9_L_v2",
    "MONO_10_9_L_v2",
    "EO_10_9_L_v2",
    "BASO_10_9_L_v2",
    "NEUT_PCT_v2",
    "LYMPH_PCT_v2",
    "MONO_PCT_v2",
    "EO_PCT_v2",
    "BASO_PCT_v2",
    "IG_10_9_L_v2",
    "IG_PCT_v2",
    "RET_PCT_v2",
    "RET_10_6_uL_v2",
    "IRF_PCT_v2",
    "LFR_PCT_v2",
    "MFR_PCT_v2",
    "HFR_PCT_v2",
    "RET_He_pg_v2",
    "IPF_v2",
    "PLT_I_10_9_L_v2",
    "MicroR_PCT_v2",
    "MacroR_PCT_v2",
    "TNC_10_9_L_v2",
    "WBC_N_10_9_L_v2",
    "TNC_N_10_9_L_v2",
    "BA_N_10_9_L_v2",
    "BA_N_PCT_v2",
    "WBC_D_10_9_L_v2",
    "TNC_D_10_9_L_v2",
    "NEUTx_10_9_L_v2",
    "NEUTx_PCT_v2",
    "LYMP_10_9_L_v2",
    "LYMP_PCT_v2",
    # "HFLC_10_9_L_v2",
    # "HFLC_PCT_v2",
    "BA_D_10_9_L_v2",
    "BA_D_PCT_v2",
    "NE_SSC_ch_v2",
    "NE_SFL_ch_v2",
    "NE_FSC_ch_v2",
    "LY_X_ch_v2",
    "LY_Y_ch_v2",
    "LY_Z_ch_v2",
    "MO_X_ch_v2",
    "MO_Y_ch_v2",
    "MO_Z_ch_v2",
    "NE_WX_v2",
    "NE_WY_v2",
    "NE_WZ_v2",
    "LY_WX_v2",
    "LY_WY_v2",
    "LY_WZ_v2",
    "MO_WX_v2",
    "MO_WY_v2",
    "MO_WZ_v2",
    "RBC_O_10_12_L_v2",
    "PLT_O_10_9_L_v2",
    "RBC_He_pg_v2",
    "Delta_He_pg_v2",
    "RET_Y_ch_v2",
    "RET_RBC_Y_ch_v2",
    "IRF_Y_ch_v2",
    # "FRC_10_12_L_v2",
    # "FRC_PCT_v2",
    "HYPO_He_PCT_v2",
    "HYPER_He_PCT_v2",
    "RPI_v2",
    "RET_UPP_v2",
    "RET_TNC_v2",
    "PLT_F_10_9_L_v2",
    "H_IPF_v2",
    "IPFx_10_9_L_v2",
]

###########################
# FUNCTIONS
###########################


###########################
# MAIN SCRIPT
###########################

if __name__ == "__main__":
    parser = argparse.ArgumentParser("Handle input and output files.")
    parser.add_argument(
        "-in_file",
        help="location of input file",
        default="../study_data/INTERVALdata_14MAY2020.csv",
        type=str,
    )
    parser.add_argument(
        "-out_file",
        help="path to outputfile to be generated",
        default="../processed_data/INTERVAL_filtered.csv",
        type=str,
    )

    args = parser.parse_args()
    data_path = args.in_file

    # import raw INTERVAL
    df_raw = pd.read_csv(data_path, encoding="latin1")
    df_raw.set_index("identifier", inplace=True)
    # dropping all entries for which df_raw has duplicate indices
    df_raw = df_raw[~df_raw.index.duplicated(keep="first")]
    # drop NaNs in times (removes quite a lot of rows! ~9000)
    # get one dataframe for visit 1 and one for visit 2
    visit_1 = pd.DataFrame()
    for feature in feats_v1:
        visit_1[feature] = df_raw[feature]
    visit_1[
        [
            "Sex",
            "AppointmentTime",
            "AppointmentDate",
            "ProcessDate",
            "ProcessTime",
            "nDonation_hx2",
            "nDonation_hx5",
        ]
    ] = df_raw[
        [
            "sexPulse",
            "appointmentTime_v1",
            "donationDate_v1",
            "processDate_v1",
            "processTime_v1",
            "nDonation_hx2",
            "nDonation_hx5",
        ]
    ]
    visit_1["Visit"] = "Visit 1"

    visit_2 = pd.DataFrame()
    for feature in feats_v2:
        visit_2[feature] = df_raw[feature]
    visit_2[
        [
            "Sex",
            "AppointmentTime",
            "AppointmentDate",
            "ProcessDate",
            "ProcessTime",
            "nDonation_hx2",
            "nDonation_hx5",
        ]
    ] = df_raw[
        [
            "sexPulse",
            "appointmentTime_v2",
            "donationDate_v2",
            "processDate_v2",
            "processTime_v2",
            "nDonation_hx2",
            "nDonation_hx5",
        ]
    ]
    visit_2["Visit"] = "Visit 2"

    visit_1.dropna(axis=0, inplace=True)
    visit_2.dropna(axis=0, inplace=True)

    # rename to lose visit number
    for column in visit_1.columns:
        # remove the _v1 if it ends in _v1
        if column[-2:] == "v1":
            visit_1.rename(columns={column: column[:-3]}, inplace=True)

    for column in visit_2.columns:
        # remove the _v2 if it ends in _v2
        if column[-2:] == "v2":
            visit_2.rename(columns={column: column[:-3]}, inplace=True)

    # join to one dataframe with one FBC per row
    df = pd.concat([visit_1, visit_2])

    # keep only within suggested limits (Vis & Huisman, 2016)
    df = df[df["HGB_g_dL"] >= 7.0]
    df = df[(df["WBC_10_9_L"] >= 2.0) & (df["WBC_10_9_L"] <= 40.0)]
    df = df[df["PLT_F_10_9_L"] >= 50.0]

    df = df[df["NEUT_10_9_L"] >= 0.5]

    # add subject sex as column
    df["Sex"] = df_raw["sexPulse"]
    df.loc[df["Sex"] == 1, "Sex"] = "M"
    df.loc[df["Sex"] == 2, "Sex"] = "F"

    # add subject age as column
    df["Age"] = df_raw["agePulse"]
    # add subject height as column
    df["Height"] = df_raw["ht"]
    # add subject weight as column
    df["Weight"] = df_raw["wt"]
    # add subject ethnicity as column
    df["Ethnicity"] = df_raw["ethnicPulse"]

    # add time columns
    df["datetime_ven"] = pd.to_datetime(
        df["AppointmentDate"] + df["AppointmentTime"], format="%d%b%Y%H:%M:%S"
    )
    df["datetime_process"] = pd.to_datetime(
        df["ProcessDate"] + df["ProcessTime"], format="%d%b%Y%H:%M:%S"
    )
    df["sampleage_h"] = (df["datetime_process"] - df["datetime_ven"]).astype(
        "timedelta64[m]"
    ) / 60.0  # use minute-precision hours as delta

    # set study startdate
    studystart = min(df.loc[:, "datetime_process"]).replace(
        hour=0, minute=0, second=0, microsecond=0
    )

    # get all the time terms for potential domain shift
    df.loc[:, "t / days"] = (df.loc[:, "datetime_process"] - studystart).astype(
        "timedelta64[D]"
    )

    df.loc[:, "t_day / hours"] = (
        df.loc[:, "datetime_process"] - df.loc[:, "datetime_process"].dt.floor("D")
    ).astype(
        "timedelta64[m]"
    ) / 60.0  # use minute-precision hours

    df.loc[:, "t_ven / hours"] = df.loc[:, "sampleage_h"]

    df.loc[:, "t_year / days"] = (
        df.loc[:, "datetime_process"]
        - df.loc[:, "datetime_process"].dt.to_period("Y").dt.to_timestamp()
    ).astype("timedelta64[D]")

    df.loc[:, "d_week"] = df.loc[:, "datetime_process"].dt.weekday

    # drop all rows with sample older than 36h at process
    df = df[df["sampleage_h"] <= 36]
    # drop rows with NaNs
    df.dropna(axis=0, inplace=True)
    # drop all rows with any zeros
    # df = df.loc[~(df == 0).any(axis=1)]
    # drop all entries with MPV > 13 as these are flagged as unreliable by Sysmex instruments (Sysmex only)
    df = df.loc[df.loc[:, "MPV_fL"] <= 13]
    # order by datetime_process
    df = df.sort_values(by="datetime_process")

    # save filtered and cleaned dataframe
    df.to_csv(args.out_file, index=True)
