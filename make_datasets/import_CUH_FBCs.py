#!/usr/bin/env python
# coding: utf-8
import sys

sys.path.append("../")

import numpy as np
import pandas as pd

from disentanglement.training_utils import get_data_centre_using_MAD

cuh_df = pd.read_parquet("[filepath...]/all_FBCs_with_metadata.parquet")

current_length = len(cuh_df)
print(f"Initially available FBC data from EpiCov: {current_length}")


print(
    "Initial number of unique patients:", len(cuh_df["STUDY_SUBJECT_DIGEST"].unique())
)


cuh_df = cuh_df[
    ~(
        cuh_df["Analyser ID"].isin(
            [
                "ADVIA 2120-5",
                "ADVIA 2120I 5",
            ]
        )
    )
]


print(f"Excluded by removing machines 5 and I5: {current_length - len(cuh_df)}")
current_length = len(cuh_df)
print(f"New length: {current_length}")


feats_hl = [
    "PLT#",
    "MPV",
    #     "PDW",
    "PCT",
    "RBC#",
    "MCV",
    "HCT",
    "MCH",
    "MCHC",
    "HGB",
    "RDW",
    "MONO#",
    "NEUT#",
    "EO#",
    "BASO#",
    #     "(NEUT+EO)#",
    #     "(EO+BASO)#",
    #     "(BASO+NEUT)#",
    #     "GRAN#",
    #     "NEUT%GRAN",
    #     "BASO%GRAN",
    #     "EO%GRAN",
    #     "MYELOID#",
    #     "GRAN%MYELOID",
    "LYMPH#",
    "WBC#",
    "MONO%",
    "NEUT%",
    "EO%",
    "BASO%",
    "LYMPH%",
]


cuh_df.dropna(subset=feats_hl, inplace=True)


print(
    f"Excluded by removing NaNs in the high-level FBCs: {current_length - len(cuh_df)}"
)
current_length = len(cuh_df)
print(f"New length: {current_length}")


# drop all rows with sample older than 36h at process
cuh_df = cuh_df[(cuh_df["sampleage_h"] <= 36) & (cuh_df["sampleage_h"] > 0)]


print(
    f"Excluded by keeping only sample ages between 0 and 36 hours (many rows have wrong/no datetime recordings): {current_length - len(cuh_df)}"
)
current_length = len(cuh_df)
print(f"New length: {current_length}")


cuh_df = cuh_df[cuh_df["Sex"].isin(["Male", "Female"])]


print(
    f"Excluded by removing sexes other than male or female: {current_length - len(cuh_df)}"
)
current_length = len(cuh_df)
print(f"New length: {current_length}")


cuh_df = cuh_df[
    get_data_centre_using_MAD(
        X=None,
        y=cuh_df[["Age"]].values,
        return_mask=True,
    )
]


print(
    f"Excluded by removing samples where subject age is over 3.5 MADs away from the median: {current_length - len(cuh_df)}"
)
current_length = len(cuh_df)
print(f"New length: {current_length}")


cuh_df = cuh_df[
    get_data_centre_using_MAD(
        X=None, y=cuh_df[feats_hl].values, return_mask=True, thresh=5.0
    )
]


print(
    f"Excluded by removing samples where any of the high-level FBC features is over 5 MADs away from the median: {current_length - len(cuh_df)}"
)
current_length = len(cuh_df)
print(f"New length: {current_length}")


cuh_df = cuh_df.groupby(["STUDY_SUBJECT_DIGEST", "TestYear"]).first().reset_index()


print(
    f"Excluded by keeping only the first test per year of every subject: {current_length - len(cuh_df)}"
)
current_length = len(cuh_df)
print(f"New length: {current_length}")


bins = np.linspace(0, 36, 11)
labels = np.arange(len(bins) - 1)
cuh_df["vendelay"] = pd.cut(cuh_df.loc[:, "sampleage_h"], bins=bins, labels=labels)


cuh_df.loc[:, "t_year / days"] = (
    cuh_df.loc[:, "datetime_process"]
    - cuh_df.loc[:, "datetime_process"].dt.to_period("Y").dt.to_timestamp()
).astype("timedelta64[D]")


# Convert the integer values in the TestMonth column to their corresponding string month names using the dt.month_name() method and the TestYear column for the year value
cuh_df["Season"] = cuh_df.apply(
    lambda x: pd.Timestamp(year=x["TestYear"], month=x["TestMonth"], day=1).strftime(
        "%B"
    ),
    axis=1,
)


bins = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, float("inf")]
# bins = np.array([0., 15., 25., 65., 200])
labels = [
    "0-10",
    "10-20",
    "20-30",
    "30-40",
    "40-50",
    "50-60",
    "60-70",
    "70-80",
    "80-90",
    "90-100",
    "100+",
]
cuh_df["age_bracket"] = pd.cut(cuh_df.loc[:, "Age"], bins=bins, labels=labels)


cuh_df = cuh_df.rename(columns={"TestSubmitter": "Test Submitter"})


cuh_df.loc[
    cuh_df["Test Submitter"].str.startswith("GP"), "Test Submitter"
] = "Primary care"
cuh_df.loc[
    ~(cuh_df["Test Submitter"] == "Primary care"), "Test Submitter"
] = "Secondary care"


print("Final number of unique patients:", len(cuh_df["STUDY_SUBJECT_DIGEST"].unique()))

cuh_df.to_csv("[filepath...]/HL-FBC_EpiCov_withoutMac5.csv")
